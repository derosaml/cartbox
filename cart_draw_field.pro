;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_draw_field - This routine draws an image of a magnetogram with field
;                    lines
;
;  usage:  cart_draw_field,ax=ax,ay=ay,az=az,open=open,mag=mag,imsc=imsc,
;            thick=thick,file=file,outim=outim,/nolines,/drawopen,/drawclosed,
;            /onscreen,/for_ps,/quiet
;          where data = structure named cart_data_block, defined in
;                       cart_data_block__define.pro, that contains the
;                       cartesian field data
;                (ax,ay,az) = angles of rotation about the x,y,and z axes in
;                             degrees, with rotation about the z axis
;                             occurring first, followed by rotation about the
;                             x axis and then rotation about the y axis
;                             (default=0 degrees for both, i.e. top-down view)
;                open = output array indicating polarity of each field line 
;                       (-1=negative, 0=closed, 1=positive)
;                mag = magnification of final image (default=1)
;                imsc = data value(s) to which to scale central magnetogram 
;                       image (default = min/max of image)
;                thick = thickness of field lines
;                file = if set, FITS files of image(s) are created and no
;                       screen output is displayed, routine automatically
;                       adds .fits extension to filename and sets onscreen to
;                       false
;                outim = on output, image of z-buffer is read into this
;                        variable
;                onscreen = if set, then display image onscreen
;                for_ps = if set, then interchange white and black colors
;                nolines = if set, no field lines are drawn, supersedes both
;                          the drawopen and drawclosed keywords
;                drawopen = if set, only open field lines are drawn
;                drawclosed = if set, only closed field lines are drawn
;                quiet = set to inhibit screen output
;                help = set to display calling syntax
;
;          and in the common block we have
;                bz = z-component of magnetic field
;                (ptx,pty,ptz) = on input, contains a (n,stepmax)-array of
;                                field line coordinates
;                nstep = an n-vector (where n=number of field lines) 
;                        containing the number of points comprising each 
;                        field line
;                rimage = on output, image of z-buffer is read into this
;                         variable
;
;  M.DeRosa -  6 Dec 2004 - created, patterned after pfss_draw_field
;             20 Feb 2005 - replaced data common block with data structure
;             13 May 2005 - added ay keyword
;             24 Jan 2006 - thick keyword now applies to boundary lines
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_draw_field,data,ax=ax,ay=ay,az=az,open=open,mag=mag,imsc=imsc,$
  thick=thick,file=file,outim=outim,nolines=nolines,drawopen=drawopen,$
  drawclosed=drawclosed,onscreen=onscreen,for_ps=for_ps,quiet=quiet,help=help

;  usage message
if keyword_set(help) or n_params() eq 0 then begin
  print,'  cart_draw_field,data,ax=ax,ay=ay,az=az,open=open,mag=mag,'+$
    'imsc=imsc,thick=thick,file=file,outim=outim,/nolines,/drawopen,'+$
    '/drawclosed,/onscreen,/for_ps,/quiet'
  return
endif

;  some error checking
if n_elements(ax) eq 0 then ax=0.
if n_elements(ay) eq 0 then ay=0.
if n_elements(az) eq 0 then az=0.
if n_elements(mag) eq 0 then mag=1
if n_elements(file) ne 0 then begin
  onscreen=0
  if size(file,/type) ne 7 then file='test'
endif
if n_elements(thick) eq 0 then thick=1
if (n_elements(drawclosed) eq 0) and (n_elements(drawopen) eq 0) then begin
  drawclosed=1b
  drawopen=1b
endif
if n_elements(drawclosed) ne 0 $
  then drawclosed=drawclosed gt 0 else drawclosed=0b
if n_elements(drawopen) ne 0 $
  then drawopen=drawopen gt 0 else drawopen=0b

;  set colors, corresponds to ANA color table 47
gre=250b
blu=252b
whi=254b
bla=0b
if keyword_set(for_ps) then miss=whi else miss=bla
if keyword_set(for_ps) then linecol=bla else linecol=whi

;  preliminaries
xmin=min(*data.xix,max=xmax)
ymin=min(*data.yix,max=ymax)
zmin=min(*data.zix,max=zmax)
npt=n_elements(*data.nstep)  ;  npt = number of field lines to be drawn

;  set up transformation matrices and scaling
!x.s=[-xmin,0.5]/(xmax-xmin)
!y.s=[-ymin,0.5]/(xmax-xmin)
!z.s=[-zmin,0.5]/(xmax-xmin)
set_plot,'z'
device,set_resolution=round(mag*[data.nx,data.ny]*2)
erase,miss
t3d,/reset,trans=-[0.5,0.5,0.25]
t3d,rotate=[0,0,az] & t3d,rotate=[ax,0,0] & t3d,rotate=[0,ay,0]
t3d,trans=[0.5,0.5,0.5]

;  get warped image of lower image (code stolen from IDL's show3 procedure) 
scim,(*data.bz)(*,*,0),outim=bzbot,sc=imsc,/quiet,top=248,/nowin,$
  white=keyword_set(for_ps)
xorig = (*data.xix)([0,data.nx-1,0,data.nx-1])	;4 corners X locns in image
yorig = (*data.yix)([0,0,data.ny-1,data.ny-1])	;4 corners Y locns
xc = xorig * !x.s[1] + !x.s[0]	;Normalized X coord
yc = yorig * !y.s[1] + !y.s[0]	;Normalized Y
p = [[xc],[yc],[0,0,0,0],[1,1,1,1]] # !P.T 
u = p[*,0]/p[*,3] * !d.x_vsize	;Scale U coordinates to device
v = p[*,1]/p[*,3] * !d.y_vsize	;and V
u0 = min(u) & v0 = min(v)		;Lower left corner of screen box
su = round(max(u)- u0+1) & sv = round(max(v) - v0+1)	;Size of new image
polywarp,[0,data.nx,0,data.nx],[0,0,data.ny,data.ny],u-u0,v-v0,1,kx,ky
bzbotim=poly_2d(bzbot,kx,ky,1,su,sv,missing=miss)
tv,bzbotim,u0,v0,xsiz=su,ysiz=sv,order=0

;  draw the individual field lines
if not keyword_set(nolines) then begin
  open=intarr(npt)
  for i=0,npt-1 do begin

    ;  determine whether line is open or closed 
    ns=(*data.nstep)(i)
    if ((*data.linekind)(i) eq 2) or ((*data.linekind)(i) eq 5) then begin
      ixc=get_interpolation_index(*data.xix,(*data.ptx)(0,i))
      iyc=get_interpolation_index(*data.yix,(*data.pty)(0,i))
      izc=get_interpolation_index(*data.zix,(*data.ptz)(0,i))
      bzc=interpolate(*data.bz,ixc,iyc,izc)
      if bzc gt 0 then open(i)=1 else open(i)=-1
    endif  ;  else open(i)=0, which has already been done

    ;  only plot those lines that go higher than the first radial gridpoint
    heightflag=max((*data.ptz)(0:ns-1,i)) gt (*data.zix)(1)
    drawflag=(drawopen and (open(i) ne 0)) or (drawclosed and (open(i) eq 0))
    if (heightflag and drawflag) then begin

      ;  determine color
      case open(i) of
        -1: col=blu
         0: col=linecol
         1: col=gre
      endcase
 
      ;  plot lines
      xpp=(*data.ptx)(0:ns-1,i)
      ypp=(*data.pty)(0:ns-1,i)
      zpp=(*data.ptz)(0:ns-1,i)
      plots,xpp,ypp,zpp,col=col,thick=thick,/t3d,/data

    endif

  endfor

endif

;  overplot boundary lines
cornerx=[xmin,xmin,xmax,xmax,xmin,xmin,xmax,xmax]
cornery=[ymin,ymax,ymax,ymin,ymin,ymax,ymax,ymin]
cornerz=[zmin,zmin,zmin,zmin,zmax,zmax,zmax,zmax]
cube=[0,1,2,3,7,6,5,4,7,3,0,4,5,1,2,6]
plots,cornerx(cube),cornery(cube),cornerz(cube),/t3d,/data,col=linecol, $
  thick=thick*0.5

;  capture image
ptr_free,data.rimage  &  data.rimage=ptr_new((outim=tvrd()))
set_plot,'x'
if keyword_set(onscreen) then begin
  re=[bindgen(250),0b,0b,255b,255b,255b,255b]
  gr=[bindgen(250),255b,255b,0b,0b,255b,255b]
  bl=[bindgen(250),0b,0b,255b,255b,255b,255b]
  tvlct,re,gr,bl
  scim,outim,win=2,quiet=quiet
endif else begin
  if keyword_set(file) then writefits,file+'.fits',outim
endelse

end

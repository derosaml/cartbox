;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_get_cross.pro - This function returns the vector cross product
;                       of two vectors (v1 x v2)
;
;  usage:  result = cart_get_cross(v1x,v2x,v1y,v2y,v1z,v2z)
;          where result = cross product of input data
;                v1x = input vector field 1, either a single array dimensioned
;                     (...,3) with the three planes are arranged
;                     (x,y,z), or the x-component of vector field 1
;                v2x = input vector field 2, either a single array dimensioned
;                     (...,3) with the three planes are arranged
;                     (x,y,z), or the x-component of vector field 2
;                v1y = the y-component of vector field 1, required if only the
;                       x-component is in v1x
;                v2y = the y-component of vector field 2, required if only the
;                       x-component is in v2x
;                v1z = the z-component of vector field 1, required if only the
;                       x-component is in v1x
;                v2z = the z-component of vector field 2, required if only the
;                       x-component is in v2x
;
;  M.DeRosa - 9 Dec 2004 - created
;             1 Mar 2005 - now accepts input vector in component form
;             8 Feb 2007 - got rid of xix,yix,zix inputs - no
;                          derivatives are taken so it doesn't really
;                          matter as long as the two arrays are
;                          dimensioned equally
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
function cart_get_cross,v1x,v2x,v1y,v2y,v1z,v2z

;  usage_message, parameter check
case n_params() of
  2:  comp=0  ;  input not in component form
  6:  comp=1  ;  input is in component form
  else: begin
    print,'  result=cart_get_cross(v1x,v2x,v1y,v2y,v1z,v2z)'
    return,-1
    end
endcase

;  check to see if dimensioned properly
nax1=size(v1x,/dim)
nax2=size(v2x,/dim)
if round(total(nax1 ne nax2)) gt 0 then begin
  print,'  cart_get_cross:  input vectors are not dimensionally compatible'
  return,-1
endif else nax=nax1
if comp then begin
  if total(size(v1x,/dim) ne size(v1y,/dim)) gt 0 then begin
    print,'  cart_get_curl:  vecx and vecy inputs are not dimensioned the same'
    return,-1
  endif
  if total(size(v1x,/dim) ne size(v1z,/dim)) gt 0 then begin
    print,'  cart_get_curl:  vecx and vecz inputs are not dimensioned the same'
    return,-1
  endif
  if total(size(v1x,/dim) ne size(v2y,/dim)) gt 0 then begin
    print,'  cart_get_curl:  vecx and vecy inputs are not dimensioned the same'
    return,-1
  endif
  if total(size(v1x,/dim) ne size(v2z,/dim)) gt 0 then begin
    print,'  cart_get_curl:  vecx and vecz inputs are not dimensioned the same'
    return,-1
  endif
endif

;  determine number of fields, and dimension result
if comp then begin
  arrprod=product(nax,/pres)
  res=make_array([arrprod,3],/noz,/float)  ;  dimension output array
  res(*,0)=reform(v1y*v2z-v1z*v2y,arrprod,/overwrite)  ;  x-component
  res(*,1)=reform(v1z*v2x-v1x*v2z,arrprod,/overwrite)  ;  y-component
  res(*,2)=reform(v1x*v2y-v1y*v2x,arrprod,/overwrite)  ;  z-component
  res=reform(res,[nax,3],/overwrite)

endif else begin

  ndim=n_elements(nax)
  npos=product(nax(0:ndim-2),/pres)

  ;  dimension arrays properly  
  res=make_array([npos,3],/noz,/float)
  v1x=reform(v1x,[npos,3],/overwrite)
  v2x=reform(v2x,[npos,3],/overwrite)

  ;  do the cross product
  res(*,0)=v1x(*,1)*v2x(*,2)-v1x(*,2)*v2x(*,1)
  res(*,1)=v1x(*,2)*v2x(*,0)-v1x(*,0)*v2x(*,2)
  res(*,2)=v1x(*,0)*v2x(*,1)-v1x(*,1)*v2x(*,0)

  ;  reform everything back
  res=reform(res,nax,/overwrite)
  v1x=reform(v1x,nax,/overwrite)
  v2x=reform(v2x,nax,/overwrite)

endelse

return,res

end

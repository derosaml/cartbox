;  creates structure that allows sharing of cartesial data between the
;  routines in the package

pro cart_data_block__define

;  dummy structure definition, use pointers so that array sizes can be
;  dynamically defined
cart_data={cart_data_block,$
  bx:ptr_new(),by:ptr_new(),bz:ptr_new(),nx:1l,ny:1l,nz:1l,$
  xix:ptr_new(),yix:ptr_new(),zix:ptr_new(),$
  stx:ptr_new(),sty:ptr_new(),stz:ptr_new(),$
  ptx:ptr_new(),pty:ptr_new(),ptz:ptr_new(),nstep:ptr_new(),$
  rimage:ptr_new(),linekind:ptr_new()}

end

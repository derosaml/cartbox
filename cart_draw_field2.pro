;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_draw_field2 - This routine draws an image of a magnetogram with field
;                     lines in a box.  Does basically the same thing
;                     as cart_draw_field.pro but uses object graphics to get
;                     the line crossings more accurate
;
;  usage:  cart_draw_field2,data,multi,xsize=xsize,ysize=ysize,
;            ax=ax,ay=ay,az=az,
;            mag=mag,imsc=imsc,thick=thick,file=file,outim=outim,
;            /nolines,/drawopen,/drawclosed,/onscreen,/for_ps,/quiet,
;            extra_objects=extra_objects
;          where data = structure named cart_data_block, defined in
;                       cart_data_block__define.pro, that contains the
;                       cartesian field data
;                multi = [ncol,nrow] array describing placement of the 
;                        multiple datasets in the widget, defaults to 2 rows
;                        if not provided, unless there is only one dataset in
;                        which case it occupies the whole view
;                xsize = number of pixels in the width of the space allocated 
;                        to one box (default=400, max=1000)
;                ysize = number of pixels in the height of the space allocated
;                        to one box (default=400, max=800)
;                [ax,ay,az] = angles of rotation about the x,y,and z axes in
;                             degrees, with rotation about the z axis
;                             occurring first, followed by rotation about the
;                             x axis and then rotation about the y axis
;                             (default=0 degrees for both, i.e. top-down view)
;                mag = magnification factor
;                imsc = data value(s) to which to scale central magnetogram 
;                       image (default = min/max of image)
;                thick = thickness of field lines
;                file = if set, FITS files of image(s) are created and no
;                       screen output is displayed, routine automatically
;                       adds .fits extension to filename and sets onscreen to
;                       false
;                outim = on output, image of z-buffer is read into this
;                        variable
;                onscreen = if set, then display image onscreen
;                for_ps = if set, then interchange white and black colors
;                nolines = if set, no field lines are drawn, supersedes both
;                          the drawopen and drawclosed keywords
;                drawopen = if set, only open field lines are drawn
;                drawclosed = if set, only closed field lines are drawn
;                quiet = set to inhibit screen output
;                help = set to display calling syntax
;                extra_objects = extra objects to be added to the views
;
;  M.DeRosa - 16 Apr 2008 - created, patterned from cart_trackball.pro
;              6 May 2009 - now does /onscreen properly
;             29 Aug 2013 - now does /for_ps properly
;             24 Sep 2013 - now deals with thick keyword properly
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_draw_field2,data,multiview,xsize=xsize,ysize=ysize,ax=ax,ay=ay,az=az,$
  mag=mag,imsc=imsc,thick=thick,file=file,outim=outim,nolines=nolines,$
  drawopen=drawopen,drawclosed=drawclosed,onscreen=onscreen,for_ps=for_ps,$
  quiet=quiet,help=help,extra_objects=extra_objects

;  usage message
if keyword_set(help) or n_params() eq 0 then begin
  print,'  cart_draw_field2,data,multi,xsize=xsize,ysize=ysize,'+$
    'ax=ax,ay=ay,az=az,'+$
    'mag=mag,imsc=imsc,thick=thick,file=file,outim=outim,/nolines,/drawopen,'+$
    '/drawclosed,/onscreen,/for_ps,/quiet,extra_objects=extra_objects'
  return
endif

;  some error checking
if n_elements(xsize) ne 1 then xsize=512 else xsize=xsize(0)
if n_elements(ysize) ne 1 then ysize=512 else ysize=ysize(0)
if n_elements(ax) eq 0 then ax=0.
if n_elements(ay) eq 0 then ay=0.
if n_elements(az) eq 0 then az=0.
if n_elements(mag) eq 0 then mag=1 else mag=mag(0)
;if n_elements(imsc) eq 0 then imsc=imsc(0)
if n_elements(file) ne 0 then begin
  onscreen=0
  if size(file,/type) ne 7 then file='test'
endif
if n_elements(thick) eq 0 then thick=1 else thick=thick(0)
if (n_elements(drawclosed) eq 0) and (n_elements(drawopen) eq 0) then begin
  drawclosed=1b
  drawopen=1b
endif
if n_elements(drawclosed) ne 0 $
  then drawclosed=drawclosed gt 0 else drawclosed=0b
if n_elements(drawopen) ne 0 $
  then drawopen=drawopen gt 0 else drawopen=0b

;  count extra objects
nextra=n_elements(extra_objects)

;  get number of views
nview=n_elements(data)
if nview eq 1 then multiview=[1,1] else $
  if n_elements(multiview) ne 2 then multiview=[2,nview/2]  ;  [col,row]
nview=nview<round(product(multiview))

;  set initial window size
xwid=xsize
ywid=ysize

;  set subwindow sizes
xwidv=xwid/multiview(0)  ;  integer division
ywidv=ywid/multiview(1)  ;  integer division

;  set RGB triplets of line colors
bla=byte([  0,  0,  0])
yel=byte([255,255,  0])
gre=byte([  0,255,  0])
mgn=byte([255,  0,255])
whi=byte([255,255,255])
blu=byte([  0,  0,255])

;  get ranges of x,y,z
xra=(yra=(zra=dblarr(2,nview)))
for i=0,nview-1 do begin
  xra1=minmax(*data(i).xix)  &  xra(*,i)=xra1
  yra1=minmax(*data(i).yix)  &  yra(*,i)=yra1
  zra1=minmax(*data(i).zix)  &  zra(*,i)=zra1
endfor

;  aspect ratios of input volumes
zxasp=(zra(1,*)-zra(0,*))/(xra(1,*)-xra(0,*))
yxasp=(yra(1,*)-yra(0,*))/(xra(1,*)-xra(0,*))

;  square position, lower-left and upper-right coords (x,y,z)
pos=[-1,-max(yxasp),-max(zxasp),1,max(yxasp),max(zxasp)]
posfac=0.3/max(pos)
pos=pos*posfac

;  set background color
if keyword_set(for_ps) then backcol=whi else backcol=bla

;  set boundary color
if keyword_set(for_ps) then bndcol=blu else bndcol=yel

;  scale all lines to the same set of normalized units
xs=[pos(0)*xra(1)-pos(3)*xra(0),(pos(3)-pos(0))]/(xra(1)-xra(0))
ys=[pos(1)*yra(1)-pos(4)*yra(0),(pos(4)-pos(1))]/(yra(1)-yra(0))
zs=[pos(2)*zra(1)-pos(5)*zra(0),(pos(5)-pos(2))]/(zra(1)-zra(0))

;  compute viewplane rectangle based on aspect ratio
aspect=float(xwidv)/float(ywidv)  ;  aspect ratio of subwindow
viewrect=[-1,-1,2,2]/sqrt(3.0)
if aspect gt 1 then begin
  viewrect(0)=viewrect(0)-((aspect-1.0)*viewrect(2))/2.0
  viewrect(2)=viewrect(2)*aspect
endif else begin
  viewrect(1)=viewrect(1)-(((1.0/aspect)-1.0)*viewrect(3))/2.0
  viewrect(3)=viewrect(3)/aspect
endelse
viewrect=viewrect/mag

;  set up each box
oview=objarr(nview)
omodeltop=objarr(nview)
omodelin=objarr(nview)
for j=0,nview-1 do begin

  ;  create views
  oview(j)=obj_new('IDLgrView',color=backcol,viewplane=viewrect,proj=2,$
    dim=[xwidv,ywidv],$
    loc=[(j mod multiview(0))*xwidv,(multiview(1)-j/multiview(0)-1)*ywidv])

  ;  create the model
  omodeltop(j)=obj_new('IDLgrModel')
  oview(j)->add,omodeltop(j)

  ;  put an inner model inside the outer model
  omodelin(j)=obj_new('IDLgrModel')
  omodeltop(j)->add,omodelin(j)

  ;  rotate model to desired view
  omodelin(j)->rotate,[0,0,1],az
  omodelin(j)->rotate,[1,0,0],ax
  omodelin(j)->rotate,[0,1,0],ay

  ;  next create fieldline objects
  if not keyword_set(nolines) then begin

    ;  determine whether field lines are open or closed
    nlines=n_elements(*data(j).nstep)
    open=intarr(nlines)
    for i=0,nlines-1 do begin
      if ((*data(j).linekind)(i) eq 2) or ((*data(j).linekind)(i) eq 5) $
        then begin
        ixc=get_interpolation_index(*data(j).xix,(*data(j).ptx)(0,i))
        iyc=get_interpolation_index(*data(j).yix,(*data(j).pty)(0,i))
        izc=get_interpolation_index(*data(j).zix,(*data(j).ptz)(0,i))
        bzc=interpolate(*data(j).bz,ixc,iyc,izc)
        if bzc gt 0 then open(i)=1 else open(i)=-1
      endif  ;  else open(i)=0, which has already been done
    endfor

    ;  create an object for each line and add them all to the model
    ofieldlines=objarr(nlines,/noz)
    for i=0,nlines-1 do begin

      ;  set appropriate color
      case open(i) of
        -1: col=mgn
         0: if keyword_set(for_ps) then col=bla else col=whi
         1: col=gre
      endcase

      ;  create polyline object
      ns=(*data(j).nstep)(i)-1
      ofieldlines(i)=obj_new('IDLgrPolyline',color=col,thick=thick, $
        transpose([[(*data(j).ptx)(0:ns,i)],$
                   [(*data(j).pty)(0:ns,i)],$
                   [(*data(j).ptz)(0:ns,i)]]))

      ;  set scaling
      ofieldlines(i)->setproperty,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs

    endfor

    ;  add fieldlines to the model
    omodelin(j)->add,ofieldlines

  endif

  ;  create texture map for image at lower boundary
  if not keyword_set(imsc) then imsc=max(abs(minmax((*data(j).bz)(*,*,0))))
  scim,(*data(j).bz)(*,*,0),outim=byteimage,sc=imsc,/nowin,/quiet
  otexmap=obj_new('IDLgrImage',data=byteimage)

  ;  create vertex list for planar surface
  xgrid=(*data(j).xix)#replicate(1,data(j).ny)
  ygrid=replicate(1,data(j).nx)#(*data(j).yix)
  zgrid=replicate(min(*data(j).zix),data(j).nx,data(j).ny)
  cart_vert=reform(transpose([[[xgrid]],[[ygrid]],[[zgrid]]],[2,0,1]),$
    3,data(j).nx*data(j).ny,/overwrite)

  ;  create the connectivity array
  connect=lonarr(5,data(j).nx-1,data(j).ny-1)
  for i=0,(data(j).ny-1)-1 do for k=0,(data(j).nx-1)-1 do begin
    stpt=i*data(j).nx+k  ;  index of starting point in vertex list
    connect(*,k,i)=[4,stpt,stpt+1,stpt+1+data(j).nx,stpt+data(j).nx]
  endfor

  ;  determine the texture map coordinates
  texture_coord=[(cart_vert(0,*)-min(cart_vert(0,*)))/(xra(1)-xra(0)),$
    (cart_vert(1,*)-min(cart_vert(1,*)))/(yra(1)-yra(0))]

  ;  add the texture mapped image to the model
  oplaneimage=obj_new('IDLgrPolygon',data=cart_vert,polygons=connect,col=whi,$
    style=2,texture_map=otexmap,texture_coord=texture_coord,/texture_interp,$
    xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs)
  omodelin(j)->add,oplaneimage

  ;  create objects for boundary box lines
  oboundlines=objarr(12,/noz)
  for i=0,3 do begin
    oboundlines(i+0)=obj_new('IDLgrPolyline',color=bndcol,thick=thick, $
      transpose([[xra(*,j)],$
                 [replicate(yra(i mod 2,j),2)],$
                 [replicate(zra(i/2,j),2)]]))
    oboundlines(i+4)=obj_new('IDLgrPolyline',color=bndcol,thick=thick, $
      transpose([[replicate(xra(i mod 2,j),2)],$
                 [yra(*,j)],$
                 [replicate(zra(i/2,j),2)]]))
    oboundlines(i+8)=obj_new('IDLgrPolyline',color=bndcol,thick=thick, $
      transpose([[replicate(xra(i mod 2,j),2)],$
                 [replicate(yra(i/2,j),2)],$
                 [zra(*,j)]]))
  endfor

  ;  scale boundary field lines to normalized units
  for i=0,11 do $
    oboundlines(i)->setproperty,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs

  ;  add to model
  omodelin(j)->add,oboundlines

  ;  add extra objects
  if nextra gt 0 then begin
    if nextra lt nview then begin  ;  cycle through extra objects
      nobj=j mod nextra
      extra_objects(nobj)->setproperty,xcoord_conv=xs,ycoord_conv=ys,$
        zcoord_conv=zs
      omodelin(j)->add,extra_objects(nobj),/alias
    endif else begin
      nobj=where((indgen(nextra) mod nview) eq j,numobj)
      for k=0,numobj-1 do begin
        extra_objects(nobj(k))->setproperty,xcoord_conv=xs,ycoord_conv=ys,$
          zcoord_conv=zs
        omodelin(j)->add,extra_objects(nobj(k)),/alias
      endfor
    endelse
  endif

endfor

;  render the image and read it out to an image array
obuffer=obj_new('IDLgrBuffer',dim=[xsize,ysize])
for i=0,nview-1 do obuffer->draw,oview(i)
oimage=obuffer->read()
oimage->getproperty,data=outim

;  destroy all objects
obj_destroy,oview
obj_destroy,obuffer
obj_destroy,oimage

;  onscreen
if keyword_set(onscreen) then scim,outim,/true,/quiet

end

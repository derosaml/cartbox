# CARTBOX software package for IDL

## Brief Description

This set of IDL programs can be used to visualize field lines in a Cartesian
vector field. It was developed to visualize magnetic field extrapolations of
solar active region patches, such as those produced by `fff.pro` (a force-free
field extraplolation program in the `nlfff` package in SSWIDL). The coordinate indices, vector field components, and field line data are stored together in structure of type `cart_data_block` that is defined in `cart_data_block__define.pro`.

## Sample Usage

First, define the `cart_data_block` structure and create a grid where the *x* and *y* axes span (&minus;1,1) and the *z* axis spans (0,2): 
```
npoints=21
vecfield={cart_data_block}
vecfield.xix=ptr_new(findgen(npoints)/((npoints-1)/2)-1)
vecfield.yix=vecfield.xix
vecfield.zix=ptr_new((*vecfield.xix)+1)
vecfield.nx=npoints
vecfield.ny=npoints
vecfield.nz=npoints
```

Next, define a vector field where (*B<sub>x</sub>*,*B<sub>y</sub>*,*B<sub>z</sub>*)=(&minus;*y*,&minus;*z*,*x*):
```
bcomponent=fltarr(npoints,npoints,npoints)
for zz=0,npoints-1 do for xx=0,npoints-1 do bcomponent(xx,*,zz)=-(*vecfield.yix)
vecfield.bx=ptr_new(bcomponent)
for yy=0,npoints-1 do for xx=0,npoints-1 do bcomponent(xx,yy,*)=-(*vecfield.zix)
vecfield.by=ptr_new(bcomponent)
for zz=0,npoints-1 do for yy=0,npoints-1 do bcomponent(*,yy,zz)=(*vecfield.xix)
vecfield.bz=ptr_new(bcomponent)
```

Then, use the package to choose the starting points for the field lines, and determine (integrate) the field lines intersecting each point:
```
cart_field_start_coord,vecfield,2,2
cart_trace_field,vecfield,/quiet,stepmax=10000
```

Lastly, use the trackball widget to fire up an interactive renderer (use the mouse to rotate and zoom the rendering):
```
cart_trackball,vecfield
```

## Program List

- `cart_data_block__define.pro` defines a structure of type `cart_data_block` that stores information about the field components, axis indices, and field lines.

- `cart_field_start_coord.pro` sets field line starting points (`stx`, `sty`, `stz` fields in the `cart_data_block` structure) so that field lines can be later traced (integrated) by `cart_trace_field.pro`.

- `cart_trace_field.pro` integrates a set of field lines and stores the result in the `ptx`, `pty`, `ptz` fields in the `cart_data_block` structure.

- `cart_draw_field.pro` and `cart_draw_field2.pro` render a set of field lines that have been traced by `cart_trace_field.pro`. The `cart_draw_field2.pro` routine uses IDL's object graphics capabilities and will be faster than `cart_draw_field.pro` in almost all instances.

- `cart_trackball.pro` uses IDL's object graphics in a widget that allows the user to interactively manipulate (rotate, pan, and zoom) vector field(s) stored in one or more `cart_data_block` structures. 

- `cart_get_cross.pro`, `cart_get_gradient.pro`, `cart_get_div.pro`, and `cart_get_curl.pro` perform vector calculus operations (cross product, gradient, divergence, and curl, respectively) of the input Cartesian scalar or vector arrays.

- `cart_get_vert_crossings.pro` determines the (x,y)-coordinates at which the currently traced fieldlines cross the specified z-coordinate given an input `cart_data_block` structure.

- `cart_view_create.pro` creates one or more `IDLgrView` objects from a set of input `cart_data_block` structure(s), which can be fed to `trackball_wrapper.pro` for interactive visualization.

- `cart_draw_view.pro` draws one or more `IDLgrView` objects, such as those created by `cart_view_create.pro`.

- `trackball_wrapper.pro` takes as input a set of `IDLgrView` objects, such as those created by `cart_view_create.pro`.


;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_get_vert_crossings - This procedure determines the (x,y)-coordinates
;                            at which the currently traced fieldlines cross
;                            the specified z-coordinate.
;
;  usage:  cart_get_vert_crossings,data,zcut
;       where data = structure of type cart_data_block that contains the
;                    cartesian field data
;             zcut = the z-coordinate at which to evaluate the line crossings
;             crossxy = on output, a [2,n] array (where n is the number of
;                       crossings) containing the (x,y) coordinates of each
;                       crossing
;             crossindex = on output, a [2,n] array (where n is the number of
;                          crossings) containing the line number and
;                          interpolated index of the crossing
;
;  notes: -output variables set to -1 if no crossings were found
;
;  M.DeRosa - 17 Oct 2006 - created
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro cart_get_vert_crossings,data,zcut,crossxy,crossindex

;  preliminaries
if ptr_valid(data.stz) then begin
  nlines=n_elements(*data.stz)
endif else begin
  print,'  cart_get_vert_crossings: no lines appear to be traced'
  return
endelse

;  check zcut
if n_elements(zcut) eq 0 then begin
  zcut=(*data.zix)(0)
  print,'  cart_get_vert_crossings: zcut set to ',zcut
endif

;  set up output arrays (these entries removed at end)
crossxy=[0,0]
crossindex=[0,0]

;  loop through each line
for i=0,nlines-1 do begin

  ;  extract coordinates of current fieldline
  ns=(*data.nstep)(i)
  lx=(*data.ptx)(0:ns-1,i)
  ly=(*data.pty)(0:ns-1,i)
  lz=(*data.ptz)(0:ns-1,i)

  ;  determine whether each point is below, at, or above zcut (-1,0,1, resp.)
  llt=lz lt zcut  ;  mask below
  lgt=lz gt zcut  ;  mask above
  lh=-1*llt + 1*lgt
  wheq=where(lz eq zcut,nwheq)
  if nwheq gt 0 then lh(wheq)=0
  
  ;  determine the (x,y) coordinates where crossings occur
  cflag=lh(0:ns-2)*lh(1:ns-1)
  whflag=where(cflag le 0,nwh)
  if nwh gt 0 then begin
    for j=0,nwh-1 do begin
      if cflag(whflag(j)) eq 0 then begin  ;  exact crossing
        j=j+1
        xy=[lx(whflag(j)),ly(whflag(j))]
        ix=[i,whflag(j)]
      endif else begin  ;  need to interpolate
        intix=(zcut-lz(whflag(j)))/(lz(whflag(j)+1)-lz(whflag(j)))
        xy=[interpolate(lx,whflag(j)+intix),interpolate(ly,whflag(j)+intix)]
        ix=[i,whflag(j)+intix]
      endelse
      crossxy=[[crossxy],[xy]]
      crossindex=[[crossindex],[ix]]
    endfor
  endif

endfor

;  set output variables to -1 if no crossings were found
if n_elements(crossxy) eq 2 then begin
  crossxy=-1
  crossindex=-1
endif else begin
  crossxy=crossxy(*,1:*)
  crossindex=crossindex(*,1:*)
endelse

end

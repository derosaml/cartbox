;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_get_gradient.pro - This function returns the gradient of a scalar
;
;  usage:  result = cart_get_gradient(xix,yix,zix,scalar,scheme=scheme)
;          where result = gradient of input data
;                xix = x index array
;                yix = y index array
;                zix = z index array
;                vecx = input data, a single array dimensioned
;                       (nx,ny,nz,nit) containing the scalar field for
;                       which the curl is desired
;                scheme = derivative scheme, passed through to dbyd1
;
;  M.DeRosa - 9 Jan 2007 - created, adapted from cart_get_curl
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
function cart_get_gradient,xix,yix,zix,scalar,scheme=scheme

;  usage_message, parameter check
if n_params() lt 4 then begin
  print,'  result=cart_get_gradient(xix,yix,zix,scalar,scheme=scheme)'
    return,-1
endif

;  check to see if dimensioned properly
nax=size(scalar,/dim)
nx=n_elements(xix)
ny=n_elements(yix)
nz=n_elements(zix)
if nx ne nax(0) then begin
  print,'  cart_get_gradient:  inputs xix and vector not compatible'
  return,-1
endif
if ny ne nax(1) then begin
  print,'  cart_get_gradient:  inputs yix and vector not compatible'
  return,-1
endif
if nz ne nax(2) then begin
  print,'  cart_get_gradient:  inputs zix and vector not compatible'
  return,-1
endif

;  determine number of fields, and dimension result
if n_elements(nax) eq 4 then nit=nax(3) else nit=1
res=fltarr(nx,ny,nz,3,nit,/noz)

;  compute gradient
for i=0,nit-1 do begin

  ;  x-derivative
  for iz=0,nz-1 do for iy=0,ny-1 do $
    res(*,iy,iz,0,i)=dbyd1(scheme=scheme,reform(scalar(*,iy,iz,i)),xix)

  ;  y-derivatives
  for iz=0,nz-1 do for ix=0,nx-1 do $
    res(ix,*,iz,1,i)=dbyd1(scheme=scheme,reform(scalar(ix,*,iz,i)),yix)

  ;  z-deriv
  for iy=0,ny-1 do for ix=0,nx-1 do $
    res(ix,iy,*,2,i)=dbyd1(scheme=scheme,reform(scalar(ix,iy,*,i)),zix)

endfor

return,reform(res,/over)

end

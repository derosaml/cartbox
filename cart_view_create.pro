;+
; NAME: cart_view_create.pro
;
; PURPOSE: To create a view object containing cartesian vector field viz
;
; CALLING SEQUENCE:
;   object_list=cart_view_create(data,multiview,/nolines,/noimage)
;
; INPUTS: data = (array of) structures named cart_data_block, defined in
;                cart_data_block__define.pro, that contains the cartesian
;                field data
;         multiview = [ncol,nrow] array describing placement of the multiple
;                     datasets in the widget, defaults to 2 rows if not
;                     provided, unless there is only one dataset in which case
;                     it occupies the whole widget
;         imsc = (optional) image scale of image
;         nolines = if set, no fieldlines are included in the model(s)
;         noimage = if set, the bottom image is not included in the model(s)
;         for_ps = if set, the views are initialized to white instead of black
;
; OUTPUTS:
;   object_list = on output, contains a structure (or an array of structures)
;                 containing all of the objects
;
; NOTES: 
;
; MODIFICATION HISTORY: 
;   M.DeRosa - 11 Jun 2008 - created, much cannibalized from cart_trackball
;              28 Jul 2008 - added for_ps option
;              12 Aug 2008 - added imsc option
;
;-

function cart_view_create,data,multiview,nolines=nolines,noimage=noimage,$
  for_ps=for_ps,imsc=imsc

;  usage message
if n_elements(data) eq 0 then begin
  print,'  object_list=cart_view_create(data,multiview,imsc=imsc,/nolines,/noimage,/for_ps)'
  return,-1
endif

;  get number of views
nview=n_elements(data)
if nview eq 1 then multiview=[1,1] else $
  if n_elements(multiview) ne 2 then multiview=[2,nview/2]  ;  [col,row]
nview=nview<round(product(multiview))

;  set initial window size
xwid=round(400*multiview(0)) < 1000
ywid=round(400*multiview(1)) <  800

;  set subwindow sizes
xwidv=xwid/multiview(0)  ;  integer division
ywidv=ywid/multiview(1)  ;  integer division

;  set RGB triplets of line colors
bla=byte([  0,  0,  0])
yel=byte([255,255,  0])
gre=byte([  0,255,  0])
blu=byte([255,  0,255])
whi=byte([255,255,255])

;  get ranges of x,y,z
xra=(yra=(zra=dblarr(2,nview)))
for i=0,nview-1 do begin
  xra1=minmax(*data(i).xix)  &  xra(*,i)=xra1
  yra1=minmax(*data(i).yix)  &  yra(*,i)=yra1
  zra1=minmax(*data(i).zix)  &  zra(*,i)=zra1
endfor

;  aspect ratios of input volumes
zxasp=(zra(1,*)-zra(0,*))/(xra(1,*)-xra(0,*))
yxasp=(yra(1,*)-yra(0,*))/(xra(1,*)-xra(0,*))

;  square position, lower-left and upper-right coords (x,y,z)
pos=[-1,-max(yxasp),-max(zxasp),1,max(yxasp),max(zxasp)]
posfac=0.3/max(pos)
pos=pos*posfac

;  scale all lines to the same set of normalized units
xs=[pos(0)*xra(1)-pos(3)*xra(0),(pos(3)-pos(0))]/(xra(1)-xra(0))
ys=[pos(1)*yra(1)-pos(4)*yra(0),(pos(4)-pos(1))]/(yra(1)-yra(0))
zs=[pos(2)*zra(1)-pos(5)*zra(0),(pos(5)-pos(2))]/(zra(1)-zra(0))

;  compute viewplane rectangle based on aspect ratio
aspect=float(xwidv)/float(ywidv)  ;  aspect ratio of subwindow
viewrect=[-1,-1,2,2]/sqrt(3.0)
if aspect gt 1 then begin
  viewrect(0)=viewrect(0)-((aspect-1.0)*viewrect(2))/2.0
  viewrect(2)=viewrect(2)*aspect
endif else begin
  viewrect(1)=viewrect(1)-(((1.0/aspect)-1.0)*viewrect(3))/2.0
  viewrect(3)=viewrect(3)/aspect
endelse

;  set up each box
oview=objarr(nview)
omodeltop=objarr(nview)
omodelin=objarr(nview)
ofieldlinesarray=ptrarr(nview)
oplaneimage=objarr(nview)
oboundlinesarray=ptrarr(nview)
if keyword_set(for_ps) then begin
  backcol=whi 
  bordcol=bla
endif else begin
  backcol=bla
  bordcol=yel
endelse
for j=0,nview-1 do begin

  ;  create views
  oview(j)=obj_new('IDLgrView',color=backcol,viewplane=viewrect,proj=1,$
    dim=[xwidv,ywidv],$
    loc=[(j mod multiview(0))*xwidv,(multiview(1)-j/multiview(0)-1)*ywidv])

  ;  create the model
  omodeltop(j)=obj_new('IDLgrModel')
  oview(j)->add,omodeltop(j)

  ;  put an inner model inside the outer model
  omodelin(j)=obj_new('IDLgrModel')
  omodeltop(j)->add,omodelin(j)

  ;  rotate model to standard view for first draw
  omodelin(j)->rotate,[1,0,0],-90
  omodelin(j)->rotate,[0,1,0],30
  omodelin(j)->rotate,[1,0,0],30

  if ~keyword_set(nolines) and ptr_valid(data(j).nstep) then begin

    ;  determine whether field lines are open or closed
    nlines=n_elements(*data(j).nstep)
    open=intarr(nlines)
    for i=0,nlines-1 do begin
      if ((*data(j).linekind)(i) eq 2) or $
         ((*data(j).linekind)(i) eq 5) then begin
        ixc=get_interpolation_index(*data(j).xix,(*data(j).ptx)(0,i))
        iyc=get_interpolation_index(*data(j).yix,(*data(j).pty)(0,i))
        izc=get_interpolation_index(*data(j).zix,(*data(j).ptz)(0,i))
        bzc=interpolate(*data(j).bz,ixc,iyc,izc)
        if bzc gt 0 then open(i)=1 else open(i)=-1
      endif  ;  else open(i)=0, which has already been done
    endfor

    ;  create an object for each line and add them all to the model
    ofieldlines=objarr(nlines,/noz)
    for i=0,nlines-1 do begin

      ;  set appropriate color
      case open(i) of
        -1: col=blu
         0: col=whi
         1: col=gre
      endcase

      ;  create polyline object
      ns=(*data(j).nstep)(i)-1
      ofieldlines(i)=obj_new('IDLgrPolyline',color=col, $
        transpose([[(*data(j).ptx)(0:ns,i)],$
                   [(*data(j).pty)(0:ns,i)],$
                   [(*data(j).ptz)(0:ns,i)]]))

      ;  set scaling
      ofieldlines(i)->setproperty,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs

    endfor

     ;  add fieldlines to the model
     omodelin(j)->add,ofieldlines
     ofieldlinesarray(j)=ptr_new(ofieldlines)

  endif

  ;  create image for lower boundary
  if ptr_valid(data(j).bz) and (not keyword_set(noimage)) then begin

    ;  create texture map for image at lower boundary
    if not keyword_set(imsc) then imsc=max(abs(minmax((*data(j).bz)(*,*,0))))
    scim,(*data(j).bz)(*,*,0),outim=byteimage,sc=imsc,/nowin,/quiet
    otexmap=obj_new('IDLgrImage',data=byteimage)

    ;  create vertex list for planar surface
    xgrid=(*data(j).xix)#replicate(1,data(j).ny)
    ygrid=replicate(1,data(j).nx)#(*data(j).yix)
    zgrid=replicate(min(*data(j).zix),data(j).nx,data(j).ny)
    cart_vert=reform(transpose([[[xgrid]],[[ygrid]],[[zgrid]]],[2,0,1]),$
      3,data(j).nx*data(j).ny,/overwrite)

    ;  create the connectivity array
    connect=lonarr(5,data(j).nx-1,data(j).ny-1)
    for i=0,(data(j).ny-1)-1 do for k=0,(data(j).nx-1)-1 do begin
      stpt=i*data(j).nx+k  ;  index of starting point in vertex list
      connect(*,k,i)=[4,stpt,stpt+1,stpt+1+data(j).nx,stpt+data(j).nx]
    endfor

    ;  determine the texture map coordinates
    texture_coord=[(cart_vert(0,*)-min(cart_vert(0,*)))/(xra(1)-xra(0)),$
      (cart_vert(1,*)-min(cart_vert(1,*)))/(yra(1)-yra(0))]

    ;  add the texture mapped image to the model
    oplaneimage(j)=obj_new('IDLgrPolygon',data=cart_vert,polygons=connect,$
      col=whi,style=2,texture_map=otexmap,texture_coord=texture_coord,$
      /texture_interp,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs)
    omodelin(j)->add,oplaneimage(j)
    
  endif

  ;  create objects for boundary box lines
  oboundlines=objarr(12,/noz)
  for i=0,3 do begin
    oboundlines(i+0)=obj_new('IDLgrPolyline',color=bordcol, $
      transpose([[xra(*,j)],$
                 [replicate(yra(i mod 2,j),2)],$
                 [replicate(zra(i/2,j),2)]]))
    oboundlines(i+4)=obj_new('IDLgrPolyline',color=bordcol, $
      transpose([[replicate(xra(i mod 2,j),2)],$
                 [yra(*,j)],$
                 [replicate(zra(i/2,j),2)]]))
    oboundlines(i+8)=obj_new('IDLgrPolyline',color=bordcol, $
      transpose([[replicate(xra(i mod 2,j),2)],$
                 [replicate(yra(i/2,j),2)],$
                 [zra(*,j)]]))
  endfor

  ;  scale boundary field lines to normalized units
  for i=0,11 do $
    oboundlines(i)->setproperty,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs

  ;  add to model
  omodelin(j)->add,oboundlines
  oboundlinesarray(j)=ptr_new(oboundlines)

endfor

return,{oview:oview,omodeltop:omodeltop,omodelin:omodelin, $
        ofieldlinesarray:ofieldlinesarray,oplaneimage:oplaneimage, $
        oboundlinesarray:oboundlinesarray}

end

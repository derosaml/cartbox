;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_trace_field - This procedure traces the vector field through a
;                     cartesian box, given an array of starting points
;
;  usage:  cart_trace_field,data,stbx,stby,stbz,stepmax=stepmax,
;            safety=safety,/endpoints,/oneway,/noreverse,/quiet
;          where data = structure named cart_data_block, defined in
;                       cart_data_block__define.pro, that contains the
;                       cartesian field data
;                (stbx,stby,stbz) = on output, (x,y,z) components of B at 
;                                   starting point
;                stepmax = max number of steps per field line (default=3000)
;                safety = maximum ds along each field line, in units of 
;                         minimum grid spacing (default = 0.5)
;                endpoints = set if endpoints only are desired
;                oneway = set if lines are to be traced in one direction only,
;                         either upward or downward depending on whether
;                         starting point is closer to the bottom or to the top
;                         of the domain, and then if the starting point is no 
;                         more than 1% away from the upper or lower boundary
;                noreverse = set if field lines are not to be reversed (by
;                         default, closed field lines are oriented so that the
;                         end with negative polarity comes first and open 
;                         field lines are oriented so that the photospheric
;                         end comes first
;                quiet = set if minimal screen output is desired
;
;          and inside the data structure:
;                (bx,by,bz) = on input, (x,y,z) components of field
;                (stx,sty,stz) = on input, contains an n-vector (where 
;                                n=number of field lines) of starting
;                                coordinates for each field line
;                (ptx,pty,ptz) = on output, contains a (n,stepmax)-array of
;                                field line coordinates
;                nstep = on output, an n-vector containing the number of 
;                        points comprising each field line
;                linekind = on output, contains kind of fieldline:
;                           1=maximum step limit reached
;                           2=line intersects inner and outer boundaries, 
;                           3=both endpoints of line lie on inner boundary, 
;                           4=both endpoints of line lie on outer boundary,
;                           5=line intersects inner and side boundaries,
;                           6=line intersects outer and side boundaries,
;                           7=both endpoints of line lie on side boundary/ies
;
;  To do: -How does one tell if line has closed on itself?  
;
;  M.DeRosa -  6 Dec 2004 - created, patterned after pfss_trace_field
;             19 Feb 2005 - replaced data common block with data structure
;              9 May 2005 - now checks if field line is at a null point (when
;                           this happens, ds=[0,0,0], but forw_euler performs
;                           just fine, however, NaNs get introduced because
;                           steplen gets increased ad infinitum -- literally!)
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_trace_field,data,stbx,stby,stbz,stepmax=stepmax,safety=safety,$
  endpoints=endpoints,oneway=oneway,noreverse=noreverse,quiet=quiet

;  usage message
if n_params() gt 5 then begin
  print,'  cart_trace_field,data,stbx,stby,stbz,stepmax=stepmax,'+$
    'safety=safety,/endpoints,/oneway,/noreverse,/quiet'
  return
endif

;  parameters
if n_elements(stepmax) eq 0 then stepmax=3000l else stepmax=long(stepmax(0))
if n_elements(safety) eq 0 then safety=0.5 else safety=float(safety(0))
npt=n_elements(*data.stz)  ;  npt = number of field lines to be drawn
xmin=min(*data.xix,max=xmax)
ymin=min(*data.yix,max=ymax)
zmin=min(*data.zix,max=zmax)

;  get delta
deltax=(*data.xix)(1)-(*data.xix)(0)
deltay=(*data.yix)(1)-(*data.yix)(0)
deltaz=(*data.zix)(1)-(*data.zix)(0)

;  initialize arrays
nstep=lonarr(npt)
linekind=intarr(npt)
stbx=(stby=(stbz=fltarr(npt)))
if keyword_set(endpoints) then begin
  ptx=fltarr(2,npt)
  pty=fltarr(2,npt)
  ptz=fltarr(2,npt)
endif else begin
  ptx=fltarr(stepmax,npt)
  pty=fltarr(stepmax,npt)
  ptz=fltarr(stepmax,npt)
endelse
ptbx=fltarr(npt,2)
ptby=fltarr(npt,2)
ptbz=fltarr(npt,2)
ptx(0,*)=*data.stx
pty(0,*)=*data.sty
ptz(0,*)=*data.stz
ix=fltarr(stepmax)
iy=fltarr(stepmax)
iz=fltarr(stepmax)

;  loop through each line
if not keyword_set(quiet) then begin
  print,'  cart_trace_field: tracing '+strcompress(npt,/r)+$
    ' field lines ...'
endif
for i=0l,npt-1 do begin

  ;  print time left
  if not keyword_set(quiet) then $
    cart_print_time,'  cart_trace_field: ',i+1,npt,tst,slen1 $
    else tst=long(systime(1))

  ;  initialize
  ix(0)=ptx(0,i)
  iy(0)=pty(0,i)
  iz(0)=ptz(0,i)
  step=1l

  ;  trace out line
  repeat begin

    ;  current point
    ptc=[ix(step-1),iy(step-1),iz(step-1)]  ;  (x,y,z) of current point

    ;  calculate value of Br,Bth,Bph at current point
    ixc=get_interpolation_index(*data.xix,ptc(0))
    iyc=get_interpolation_index(*data.yix,ptc(1))
    izc=get_interpolation_index(*data.zix,ptc(2))
    bxc=interpolate(*data.bx,ixc,iyc,izc)
    byc=interpolate(*data.by,ixc,iyc,izc)
    bzc=interpolate(*data.bz,ixc,iyc,izc)
    ds=reform([bxc,byc,bzc])

    ;  initialization
    if step eq 1 then begin

      ;  set initial fields
      ptbx(i,0)=bxc
      ptby(i,0)=byc
      ptbz(i,0)=bzc

      ;  determine of loop is to be traced both ways or one way only
      if keyword_set(oneway) then begin

        ;  determine if starting point is at top, bottom or middle
        pct=(ptc(2)-zmin)/(zmax-zmin)
        case 1 of
          pct gt 0.99: begin
            top=1
            linekind(i)=4
            end
          ((pct lt 0.01) and (float((*data.zix)(1)) ge 1.01)): begin
            top=-1
            linekind(i)=3
            end
          ((ptc(2) lt float((*data.xix)(1))) or (pct lt 0.01)): begin
            top=-1
            linekind(i)=3
            end
          else: top=0
        endcase

      endif else top=0

      ;  set initial stepsize and direction for first step
      stbx(i)=bxc  &  stby(i)=byc  &  stbz(i)=bzc
      bsign=sign_mld(bzc)
      steplen=safety*deltaz/(abs(bzc)>1.0)
      if top ge 0 then bsign=-bsign

    endif
    if bsign lt 0 then ds=-ds

    ;  take a step, repeat if too big
    stepflag=0
    repeat begin

      ;  step forward by an amount steplen along direction ds
      result=forw_euler(step-1,ptc,steplen,ds)

      ;  evaluate if current step was too big or too small
      diff=result-ptc
      err=max(abs(diff/[deltax,deltay,deltaz]))/safety
      case 1 of
        err gt 1: begin  ;  stepsize too big, reduce and do over
          steplen=0.5*steplen
          stepflag=0
          end
        err lt 0.1: begin  ;  stepsize too small, increase for next time
          steplen=2*steplen
          stepflag=1
          end
        else: stepflag=1  ;  don't change stepsize, just set flag to exit
      endcase
        
    endrep until stepflag

    ;  store in iz,iy,ix arrays
    ix(step)=result(0)
    iy(step)=result(1)
    iz(step)=result(2)

    ;  set flags
    hitsides=(ix(step) lt xmin) or (ix(step) gt xmax) or $
             (iy(step) lt ymin) or (iy(step) gt ymax)
    hittop=(iz(step) gt zmax)
    hitbot=(iz(step) lt zmin)
    hitnull=(total(ds eq 0) eq 3)
    hitstepmax=((step+1) eq stepmax)

    ;  deal with flags
    if (hitsides or hittop or hitbot or hitnull or hitstepmax) then begin
      if top eq 0 then begin
        if hitstepmax or hitnull then begin  ;  line may close on itself?
          linekind(i)=1
          flag=1
        endif else begin  ;  hit boundary

          ;  reset flag
          flag=0

          ;  set case to indicate which boundary
          case 1 of
            hittop: begin
              linekind(i)=4
              top=1
              end
            hitbot: begin
              linekind(i)=3
              top=-1
              end
            hitsides: if linekind(i) eq 7 then flag=1 else linekind(i)=7
            else: begin
              print,'  shouldn''t be able to get here'
              stop
              end
          endcase

          ;  reverse order of data and continue in other direction
          ix(0:step)=reverse(ix(0:step))
          iy(0:step)=reverse(iy(0:step))
          iz(0:step)=reverse(iz(0:step))
          
          ;  reset initial fields
          ptbx(i,0)=bxc
          ptby(i,0)=byc
          ptbz(i,0)=bzc

          ;  reset sign, step length
          bsign=-bsign

        endelse
      endif else begin
        flag=1
        linekindc=linekind(i)
        case 1 of
          hittop: begin
            case linekindc of
              3: linekind(i)=2  ;  I'm not sure this can happen
              4: linekind(i)=4
              7: linekind(i)=6
              else: begin
                print,'  shouldn''t be able to get here'
                stop
                end
            endcase
            end
          hitbot: begin
            case linekindc of
              3: linekind(i)=3  ;  I'm not sure this can happen either 
              4: begin
                linekind(i)=2
                if not keyword_set(noreverse) then begin
                  ix(0:step)=reverse(ix(0:step))
                  iy(0:step)=reverse(iy(0:step))
                  iz(0:step)=reverse(iz(0:step))
               endif
                end
              7: linekind(i)=5
              else: begin
                print,'  shouldn''t be able to get here'
                stop
                end
            endcase
            end
          hitsides: begin
            if ((linekindc eq 3) or (linekindc eq 4)) then begin
              linekind(i)=linekindc+2 
            endif else begin
              print,'  shouldn''t be able to get here'
              stop
            endelse
            end
          hitstepmax: linekind(i)=1
          hitnull: linekind(i)=1
        endcase
      endelse
    endif else flag=0

    ;  increment counter
    step=step+1

  endrep until flag
  nstep(i)=step

  ;  set final fields
  ptbx(i,1)=bxc
  ptby(i,1)=byc
  ptbz(i,1)=bzc

  ;  if closed, make field line go from negative to positive
  if not keyword_set(noreverse) then begin
    if (linekind(i) eq 3) and (ptbz(i,0) gt 0) then begin
      ix(0:step-1)=reverse(ix(0:step-1))
      iy(0:step-1)=reverse(iy(0:step-1))
      iz(0:step-1)=reverse(iz(0:step-1))
      ptbx(i,*)=reverse(ptbx(i,*))
      ptby(i,*)=reverse(ptby(i,*))
      ptbz(i,*)=reverse(ptbz(i,*))
    endif
  endif

  ;  fill pt arrays
  if keyword_set(endpoints) then begin
    ptx(*,i)=ix([0,step-1])
    pty(*,i)=iy([0,step-1])
    ptz(*,i)=iz([0,step-1])
  endif else begin
    ptx(*,i)=ix
    pty(*,i)=iy
    ptz(*,i)=iz
  endelse

endfor
ttime=round((long(systime(1))-tst)/60.)
if not keyword_set(quiet) then $
  print,'  cart_trace_field: total time = '+strcompress(ttime,/r)+' min'

;  assign pointers
maxnstep=max(nstep)
ptr_free,data.ptx,data.pty,data.ptz,data.nstep,data.linekind
if (not keyword_set(endpoints)) and (max(nstep) lt stepmax) then begin
  data.ptx=ptr_new(ptx(0:maxnstep-1,*))
  data.pty=ptr_new(pty(0:maxnstep-1,*))
  data.ptz=ptr_new(ptz(0:maxnstep-1,*))
endif else begin
  data.ptx=ptr_new(ptx)
  data.pty=ptr_new(pty)
  data.ptz=ptr_new(ptz)
endelse
data.nstep=ptr_new(nstep)
data.linekind=ptr_new(linekind)

end

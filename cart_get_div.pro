;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_get_div.pro - This function returns the divergence of a vector
;
;  usage:  result = cart_get_div(xix,yix,zix,vecx,vecy,vecz)
;          where result = divergence of input data
;                xix = x index array
;                yix = y index array
;                zix = z index array
;                vecx = input data, either a single array dimensioned
;                       (nx,ny,nz,3,nit) with the three planes are arranged
;                       (x,y,z), or the x-component of the vector field for
;                       which the divergence is desired
;                vecy = the y-component of the vector field for which the
;                       divergence is desired, required if only the
;                       x-component is in vecx
;                vecz = the z-component of the vector field for which the
;                       divergence is desired, required if only the
;                       x-component is in vecx
;                scheme = finite difference scheme to use, see dbyd1.pro
;
;  M.DeRosa - 8 Dec 2004 - created
;             1 Mar 2005 - now accepts input vector in component form
;             1 Feb 2007 - fixed indexing bug
;             7 Nov 2014 - added scheme keyword, gets passed to dbyd1.pro
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function cart_get_div,xix,yix,zix,vecx,vecy,vecz,scheme=scheme

;  usage_message, parameter check
case n_params() of
  4:  comp=0  ;  input not in component form
  6:  comp=1  ;  input is in component form
  else: begin
    print,'  result=cart_get_div(xix,yix,zix,vecx,vecy,vecz)'
    return,-1
    end
endcase

;  check to see if dimensioned properly
nax=size(vecx,/dim)
nx=n_elements(xix)
ny=n_elements(yix)
nz=n_elements(zix)
if nx ne nax(0) then begin
  print,'  cart_get_div:  inputs xix and vecx not compatible'
  return,-1
endif
if ny ne nax(1) then begin
  print,'  cart_get_div:  inputs yix and vecx not compatible'
  return,-1
endif
if nz ne nax(2) then begin
  print,'  cart_get_div:  inputs zix and vecx not compatible'
  return,-1
endif
if comp then begin
  if total(size(vecx,/dim) ne size(vecy,/dim)) gt 0 then begin
    print,'  cart_get_div:  vecx and vecy inputs are not dimensioned the same'
    return,-1
  endif
  if total(size(vecx,/dim) ne size(vecz,/dim)) gt 0 then begin
    print,'  cart_get_div:  vecx and vecz inputs are not dimensioned the same'
    return,-1
  endif
endif

;  determine number of fields, and dimension result
if comp then begin
  if n_elements(nax) eq 4 then nit=nax(3) else nit=1
endif else begin
  if n_elements(nax) eq 5 then nit=nax(4) else nit=1
endelse
res=fltarr(nx,ny,nz,nit,/noz)

;  compute divergence
if comp then begin

  for i=0,nit-1 do begin

    ;  x-deriv
    for iz=0,nz-1 do for iy=0,ny-1 do $
      ;res(*,iy,iz,i)=deriv(xix,reform(vecx(*,iy,iz,i)))
      res(*,iy,iz,i)=dbyd1(reform(vecx(*,iy,iz,i)),xix,scheme=scheme)

    ;  y-deriv
    for iz=0,nz-1 do for ix=0,nx-1 do $
      ;res(ix,*,iz,i)=res(ix,*,iz,i)+deriv(yix,reform(vecy(ix,*,iz,i)))
      res(ix,*,iz,i)=res(ix,*,iz,i)+dbyd1(reform(vecy(ix,*,iz,i)),yix,scheme=scheme)

    ;  z-deriv
    for iy=0,ny-1 do for ix=0,nx-1 do $
      ;res(ix,iy,*,i)=res(ix,iy,*,i)+deriv(zix,reform(vecz(ix,iy,*,i)))
      res(ix,iy,*,i)=res(ix,iy,*,i)+dbyd1(reform(vecz(ix,iy,*,i)),zix,scheme=scheme)

  endfor

endif else begin

  for i=0,nit-1 do begin

    ;  x-deriv
    for iz=0,nz-1 do for iy=0,ny-1 do $
      ;res(*,iy,iz,i)=deriv(xix,reform(vecx(*,iy,iz,0,i)))
      res(*,iy,iz,i)=dbyd1(reform(vecx(*,iy,iz,0,i)),xix,scheme=scheme)

    ;  y-deriv
    for iz=0,nz-1 do for ix=0,nx-1 do $
      ;res(ix,*,iz,i)=res(ix,*,iz,i)+deriv(yix,reform(vecx(ix,*,iz,1,i)))
      res(ix,*,iz,i)=res(ix,*,iz,i)+dbyd1(reform(vecx(ix,*,iz,1,i)),yix,scheme=scheme)

    ;  z-deriv
    for iy=0,ny-1 do for ix=0,nx-1 do $
      ;res(ix,iy,*,i)=res(ix,iy,*,i)+deriv(zix,reform(vecx(ix,iy,*,2,i)))
      res(ix,iy,*,i)=res(ix,iy,*,i)+dbyd1(reform(vecx(ix,iy,*,2,i)),zix,scheme=scheme)

  endfor

endelse

return,reform(res,/over)

end

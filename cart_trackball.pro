;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_trackball.pro - Creates a widget application that allows the user to
;                       manipulate the view of a set of fieldlines from one or
;                       more different datasets in real time, uses IDL's
;                       trackball widget combined with object graphics
;  usage:  cart_trackball,data,multi,imsc=imsc,/noimage,/nolines,
;                         extra_objects=extra_objects,/add_extra_objects_to_all
;          where data = (array of) structures named cart_data_block, defined
;                       in cart_data_block__define.pro, that contains the 
;                       cartesian field data
;                multi = [ncol,nrow] array describing placement of the 
;                        multiple datasets in the widget, defaults to 2 rows
;                        if not provided, unless there is only one dataset in
;                        which case it occupies the whole widget
;                imsc = data value(s) to which to scale image on bottom face 
;                       (default = min/max of image)
;                nolines = set this keyword to ignore fieldlines data that are
;                          stored in the cart_data_block structures
;                orthogonal = set this keyword for an orthogonal projection
;                             (as opposed to one with a vanishing point)
;                extra_objects = extra objects to be added to each of
;                                the views
;                add_extra_objects_to_all = if set, each of the extra objects
;                                           are added to each of the views,
;                                           otherwise they may be distributed
;                                           one per view
;
;  NOTES:  1. If the number of extra objects doesn't match the number of
;             boxes, the number of objects will be repeated so that each box
;             has an object (if there are fewer objects than boxes), or the
;             objects will wrap around giving the boxes multiple objects.
;          2. To add a line to the model (in this case a red line from one
;             corner to the opposite corner), set the extra_objects keyword to
;             oline, with oline defined as:
;                IDL> oline=obj_new('IDLgrPolyline',col=byte([255,0,0]),$
;                       transpose([[minmax(*data.xix)],$
;                       [minmax(*data.yix)],[minmax(*data.zix)]]))
;          3. To add a surface to the model (in this case a red ball centered
;             at the origin), set the extra_objects keyword to
;             osurf, with osurf defined as:
;                IDL> xgrid=rebin(reform(*data.xix,[data.nx,1,1]),$
;                       [data.nx,data.ny,data.nz])
;                IDL> ygrid=rebin(reform(*data.yix,[1,data.ny,1]),$
;                       [data.nx,data.ny,data.nz])
;                IDL> zgrid=rebin(reform(*data.zix,[1,1,data.nz]),$
;                       [data.nx,data.ny,data.nz])
;                IDL> dist=sqrt(xgrid^2+ygrid^2+zgrid^2)
;                IDL> isosurface,dist,mean(*data.zix),verts,connect
;                IDL> vertices=[interpolate(*data.xix,verts(0,*)),$
;                       interpolate(*data.yix,verts(1,*)),$
;                       interpolate(*data.zix,verts(2,*))]
;                IDL> osurf=obj_new('IDLgrPolygon',data=vertices,$
;                       polygons=connect,col=byte([255,0,0]),alpha=0.5)
;
;  M.DeRosa - 16 Feb 2004 - created, follows IDL's surf_track demo except that
;                           IDLgrPolyline is used instead of IDLgrSurface,
;                           also David Fanning's xplot.pro was very helpful
;             19 Feb 2004 - replaced data common block with data structure
;             22 Feb 2004 - now can deal with multiple datasets
;             29 Jun 2007 - added image at bottom
;             29 Jun 2007 - now zooms in (instead of out) when right-clicking
;                           and moving the mouse down, and vice versa (like
;                           Google Earth)
;              3 Jul 2007 - added the keyword to which user-defined objects
;                           can be added to the model.  See examples in the
;                           NOTES section above.
;             10 Jun 2008 - now checks to see whether BZ is valid, and if not,
;                           does not create the image at bottom of box
;             28 Jun 2008 - now has pan and zoom buttons
;              1 Jul 2011 - due to the way IDL version 8 interprets the "dot
;                           notation" wrt objects, I had to use square
;                           brackets instead of parentheses to get lineslike
;                           "olist.ofieldlines[i]->getproperty,color=col" to
;                           compile (10 places)
;              1 Jul 2011 - fixed issues with widget resize events that was
;                           causing the widget to crash in IDL v.8.0.
;             19 Mar 2014 - added /nolines keyword
;             26 Jun 2015 - added imsc keyword
;             28 Jun 2018 - if linekind is not specified, then routine now
;                           colors all lines white (vs erroring out)
;             28 Jun 2018 - added orthogonal keyword
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_trackball_event,event

;  get event ID and widget state
widget_control,event.id,get_uval=uval

if event.id eq event.top then begin  ;  TLB resize event

  ;  redefine state
  state=uval
  
  ;  get current TLB size
  widget_control,event.top,tlb_get_size=newwid

  ;  compute difference between current and old TLB size
  dwid=newwid-state.twid

  ;  set new draw window size
  state.vwid=state.vwid+dwid

  ;  determine new zoom factor
  zoomfac=min(newwid/state.twid)
  state.vzoom=state.vzoom*zoomfac

  ;  resize the draw widget
  widget_control,state.wdraw,draw_x=state.vwid(0),draw_y=state.vwid(1)

  ;  compute new subwindow sizes
  state.vwidv=state.vwid/state.vmulti  ;  integer division

  ;  compute viewplane rectangle based on aspect ratio.
  aspect=float(state.vwidv(0))/float(state.vwidv(1))
  viewrect=[-1,-1,2,2]/sqrt(3.0)  ;  at first view spans -1/2 to +1/2 in x,y
  if aspect gt 1 then begin
    viewrect(0)=viewrect(0)-((aspect-1.0)*viewrect(2))/2.0
    viewrect(2)=viewrect(2)*aspect
  endif else begin
    viewrect(1)=viewrect(1)-(((1.0/aspect)-1.0)*viewrect(3))/2.0
    viewrect(3)=viewrect(3)/aspect
  endelse
  for i=0,state.nview-1 do state.oview[i]->setproperty,viewplane=viewrect, $
    dim=state.vwidv,loc=float([(i mod state.vmulti(0))*state.vwidv(0),$
      (state.vmulti(1)-i/state.vmulti(0)-1)*state.vwidv(1)])

  ;  redraw view    
  state.owindow->draw

  ;  reset trackball
  state.otrack->reset,0.5*state.vwid,0.5*max(state.vwid)

endif else begin

  ;  get state
  widget_control,event.top,get_uval=state,/no_copy

  case (strsplit(uval,':',/extract))(0) of

    'EXIT': begin  ;  exit button pressed
      obj_destroy,state.ovgr
      widget_control,event.top,/destroy
      return
      end

    'DRAW': begin  ;  (re)draw view

      ;  deal with buttons
      if state.bmode ne 'zoom' then begin

        ;  determine whether we are panning or rotating
        if state.bmode eq 'pan' then translate=1b else translate=0b

        ;  handle trackball updates
        newview=state.otrack->update(event,transform=tmatrix,transl=translate)
        if newview gt 0 then begin
          if translate then begin  ;  apply translation to top model
            state.omodeltop->getproperty,transform=oldt
            state.omodeltop->setproperty,transform=oldt#tmatrix
          endif else begin  ;  apply rotation to inner models
            for i=0,state.nview-1 do begin
              state.omodelin[i]->getproperty,transform=oldt
              state.omodelin[i]->setproperty,transform=oldt#tmatrix
            endfor

            ;  compute rotation angles
            state.omodelin[0]->getproperty,transform=tmatrix
            yang=asin(tmatrix(2))
            if abs(cos(yang)) gt 0.005 then begin
              xang=atan(-tmatrix(6),tmatrix(10))
              zang=atan(-tmatrix(1),tmatrix(0))
            endif else begin
              xang=0.0
              zang=atan(tmatrix(4),tmatrix(5))
            endelse
            outstr='('+string(xang*!radeg,f='(i4)')+','+ $
              string(yang*!radeg,f='(i4)')+','+string(zang*!radeg,f='(i4)')+')'
            widget_control,state.wrotl,set_val='rot.ang.(x,y,z)='+outstr

          endelse

        endif

      endif

      ;  handle mouse button events within draw widget
      case event.type of  ;  button down
        0: begin
          widget_control,state.wdraw,/draw_motion_events
          state.bdowny=event.y
          state.bdownzoom=state.vzoom
          state.oview[0]->getproperty,viewplane=bdv  &  state.bdownview=bdv
          case event.press of
            1b: state.bleft=1b
            4b: state.bright=1b
            else:
          endcase
          end
        1: begin   ;  button up
          widget_control,state.wdraw,draw_motion_events=0
          state.bleft=0b  &  state.bright=0b
          end
        2: begin  ;  adjust zoom
          if (state.bright) or ((state.bleft) and (state.bmode eq 'zoom')) $
            then begin
            for i=0,state.nview-1 do begin
              zoomfac=exp((event.y-state.bdowny)/float(state.vwid(1)))
              state.vzoom=state.bdownzoom*zoomfac
              state.oview[i]->setproperty,viewpl=state.bdownview*zoomfac
            endfor
          endif
          end
        else:  ;  pass through
      endcase

      ;  (re)draw view    
      state.owindow->draw

      end

    'SAVE': begin  ;  save to disk

      ;  (re)draw view    
      state.owindow->draw

      ;  get snapshot of draw window
      state.owindow->getproperty,image_data=outim

      ;  determine filename
      ftype=(strsplit(uval,':',/extract))(1)
      fname=dialog_pickfile(file='cart_trackball.'+strlowcase(ftype),/write)

      ;  save to disk
      if fname ne '' then begin
        case ftype of
          'GIF': write_gif,fname,color_quan(outim,1,r,g,b),r,g,b
          'JPG': write_jpeg,fname,outim,/true,q=90
          'PNG': write_png,fname,outim
          'TIF': write_tiff,fname,reverse(outim,3)
          'EPS': begin  ;  clipboard idea from D.Fanning  (thanks David!)
            state.owindow->getproperty,dim=dim,units=units
            res300=replicate(2.54/300,2)  ;  300 DPI
            clip=obj_new('IDLgrClipboard',dim=dim,res=res,units=units,q=2)
            clip->draw,state.ovgr,file=fname,/postscript,/vector
            obj_destroy,clip
            end
          else:  ;  shouldn't be able to get here
        endcase
      endif

      end

    'MOUSE': begin  ;  mouse behavior events

      ;  determine what happened
      case (strsplit(uval,':',/extract))(1) of
        'PAN': state.bmode='pan'
        'ZOOM': state.bmode='zoom'
        else: state.bmode='rot'
      endcase
      
      ;  (re)draw view    
      state.owindow->draw

      end

    else: stop  ;  unrecognized event...

  endcase

  ;  set state
  widget_control,event.top,set_uval=state,/no_copy      

endelse

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_trackball,data,multiview,imsc=imsc,orthogonal=orthogonal,$
  extra_objects=extra_objects,add_extra_objects_to_all=add_extra_objects_to_all

;  usage message
if n_elements(data) eq 0 then begin
  print,'  cart_trackball,data,multi'
  return
endif

;  count extra objects
nextra=n_elements(extra_objects)

;  get number of views
nview=n_elements(data)
if nview eq 1 then multiview=[1,1] else $
  if n_elements(multiview) ne 2 then multiview=[nview/2,2]  ;  [col,row]
nview=nview<round(product(multiview))

;  set initial window size
xwid=round(400*multiview(0)) < 1000
ywid=round(400*multiview(1)) <  600

;  set subwindow sizes
xwidv=xwid/multiview(0)  ;  integer division
ywidv=ywid/multiview(1)  ;  integer division

;  set RGB triplets of line colors
bla=byte([  0,  0,  0])
yel=byte([255,255,  0])
gre=byte([  0,255,  0])
blu=byte([255,  0,255])
whi=byte([255,255,255])

;  get ranges of x,y,z
xra=(yra=(zra=dblarr(2,nview)))
for i=0,nview-1 do begin
  xra1=minmax(*data(i).xix)  &  xra(*,i)=xra1
  yra1=minmax(*data(i).yix)  &  yra(*,i)=yra1
  zra1=minmax(*data(i).zix)  &  zra(*,i)=zra1
endfor

;  aspect ratios of input volumes
zxasp=(zra(1,*)-zra(0,*))/(xra(1,*)-xra(0,*))
yxasp=(yra(1,*)-yra(0,*))/(xra(1,*)-xra(0,*))

;  square position, lower-left and upper-right coords (x,y,z)
pos=[-1,-max(yxasp),-max(zxasp),1,max(yxasp),max(zxasp)]
posfac=0.3/max(pos)
pos=pos*posfac

;  scale all lines to the same set of normalized units
xs=[pos(0)*xra(1)-pos(3)*xra(0),(pos(3)-pos(0))]/(xra(1)-xra(0))
ys=[pos(1)*yra(1)-pos(4)*yra(0),(pos(4)-pos(1))]/(yra(1)-yra(0))
zs=[pos(2)*zra(1)-pos(5)*zra(0),(pos(5)-pos(2))]/(zra(1)-zra(0))

;  create the widget
master=widget_base(/col,/base_align_left,/tlb_size_events,mbar=wmbar, $
  title='Field Line Renderer')

;  file menu
version=float(!version.release)  ;  for GIF availability test (below)
wmenuf=widget_button(wmbar,val='File ',/menu)
wsave=widget_button(wmenuf,val='Save as ...',/menu)
weps=widget_button(wsave,val='EPS',uval='SAVE:EPS')
if (version le 5.3) or (version ge 6.1) then $
  wgif=widget_button(wsave,val='GIF',uval='SAVE:GIF')
wjpeg=widget_button(wsave,val='JPEG',uval='SAVE:JPG')
wpng=widget_button(wsave,val='PNG',uval='SAVE:PNG')
wtiff=widget_button(wsave,val='TIFF',uval='SAVE:TIF')
wexitb=widget_button(wmenuf,val='Exit',uval='EXIT')

;  options under menu
woptions=widget_base(master,/row,/base_align_center)

;  rotate/pan button select
wpanb=widget_base(woptions,/row,/exclusive,frame=2)
wclickrot=widget_button(wpanb,val='rotate',uval='MOUSE:ROTATE')
wclickpan=widget_button(wpanb,val='pan',uval='MOUSE:PAN')
wclickpan=widget_button(wpanb,val='zoom',uval='MOUSE:ZOOM')
widget_control,wclickrot,/set_button  &  bmode='rot'

;  rotation angle info
wrotb=widget_base(woptions,/row)
wrotl=widget_label(wrotb,val='                                ')

;  draw window
wdrawb=widget_base(master)
wdraww=widget_draw(wdrawb,xsize=xwid,ysize=ywid,uvalue='DRAW',retain=0, $
  /expose_events,/button_events,graphics_level=2);,/color_model)

;  compute viewplane rectangle based on aspect ratio
aspect=float(xwidv)/float(ywidv)  ;  aspect ratio of subwindow
viewrect=[-1,-1,2,2]/sqrt(3.0)
if aspect gt 1 then begin
  viewrect(0)=viewrect(0)-((aspect-1.0)*viewrect(2))/2.0
  viewrect(2)=viewrect(2)*aspect
endif else begin
  viewrect(1)=viewrect(1)-(((1.0/aspect)-1.0)*viewrect(3))/2.0
  viewrect(3)=viewrect(3)/aspect
endelse

;  set up each box
oview=objarr(nview)
omodeltop=objarr(nview)
omodelin=objarr(nview)
for j=0,nview-1 do begin

  ;  create views
  oview(j)=obj_new('IDLgrView',color=bla,viewplane=viewrect, $
    proj=2-keyword_set(orthogonal),dim=[xwidv,ywidv],$
    loc=[(j mod multiview(0))*xwidv,(multiview(1)-j/multiview(0)-1)*ywidv])

  ;  create the model
  omodeltop(j)=obj_new('IDLgrModel')
  oview(j)->add,omodeltop(j)

  ;  put an inner model inside the outer model
  omodelin(j)=obj_new('IDLgrModel')
  omodeltop(j)->add,omodelin(j)

  ;  rotate model to standard view for first draw
  omodelin(j)->rotate,[1,0,0],-90
  omodelin(j)->rotate,[0,1,0],30
  omodelin(j)->rotate,[1,0,0],30

  ;  draw fieldlines
  if ~keyword_set(nolines) and ptr_valid(data(j).nstep) then begin

    ;  determine whether field lines are open or closed
    nlines=n_elements(*data(j).nstep)
    open=intarr(nlines)
    if ptr_valid(data(j).linekind) then begin
      for i=0,nlines-1 do begin
        if ((*data(j).linekind)(i) eq 2) or ((*data(j).linekind)(i) eq 5) $
          then begin
          ixc=get_interpolation_index(*data(j).xix,(*data(j).ptx)(0,i))
          iyc=get_interpolation_index(*data(j).yix,(*data(j).pty)(0,i))
          izc=get_interpolation_index(*data(j).zix,(*data(j).ptz)(0,i))
          bzc=interpolate(*data(j).bz,ixc,iyc,izc)
          if bzc gt 0 then open(i)=1 else open(i)=-1
        endif  ;  else open(i)=0, which has already been done
      endfor
    endif

    ;  create an object for each line and add them all to the model
    ofieldlines=objarr(nlines,/noz)
    for i=0,nlines-1 do begin

      ;  set appropriate color
      case open(i) of
        -1: col=blu
         0: col=whi
         1: col=gre
      endcase

      ;  create polyline object
      ns=(*data(j).nstep)(i)-1
      ofieldlines(i)=obj_new('IDLgrPolyline',color=col, $
        transpose([[(*data(j).ptx)(0:ns,i)],$
                   [(*data(j).pty)(0:ns,i)],$
                   [(*data(j).ptz)(0:ns,i)]]))

      ;  set scaling
      ofieldlines[i]->setproperty,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs

    endfor

    ;  add fieldlines to the model
    omodelin(j)->add,ofieldlines

  endif

  ;  create image for lower boundary
  if ptr_valid(data(j).bz) then begin

    ;  create texture map for image at lower boundary
    if ~keyword_set(imsc) then imsc=max(abs(minmax((*data(j).bz)(*,*,0))))
    scim,(*data(j).bz)(*,*,0),outim=byteimage,sc=imsc,/nowin,/quiet
    otexmap=obj_new('IDLgrImage',data=byteimage)

    ;  create vertex list for planar surface
    xgrid=(*data(j).xix)#replicate(1,data(j).ny)
    ygrid=replicate(1,data(j).nx)#(*data(j).yix)
    zgrid=replicate(min(*data(j).zix),data(j).nx,data(j).ny)
    cart_vert=reform(transpose([[[xgrid]],[[ygrid]],[[zgrid]]],[2,0,1]),$
      3,data(j).nx*data(j).ny,/overwrite)

    ;  create the connectivity array
    connect=lonarr(5,data(j).nx-1,data(j).ny-1)
    for i=0,(data(j).ny-1)-1 do for k=0,(data(j).nx-1)-1 do begin
      stpt=i*data(j).nx+k  ;  index of starting point in vertex list
      connect(*,k,i)=[4,stpt,stpt+1,stpt+1+data(j).nx,stpt+data(j).nx]
    endfor

    ;  determine the texture map coordinates
    texture_coord=[(cart_vert(0,*)-min(cart_vert(0,*)))/(xra(1)-xra(0)),$
      (cart_vert(1,*)-min(cart_vert(1,*)))/(yra(1)-yra(0))]

    ;  add the texture mapped image to the model
    oplaneimage=obj_new('IDLgrPolygon',data=cart_vert,polygons=connect,$
      col=whi,style=2,texture_map=otexmap,texture_coord=texture_coord,$
      /texture_interp,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs)
    omodelin(j)->add,oplaneimage

  endif

  ;  create objects for boundary box lines
  oboundlines=objarr(12,/noz)
  for i=0,3 do begin
    oboundlines(i+0)=obj_new('IDLgrPolyline',color=yel, $
      transpose([[xra(*,j)],$
                 [replicate(yra(i mod 2,j),2)],$
                 [replicate(zra(i/2,j),2)]]))
    oboundlines(i+4)=obj_new('IDLgrPolyline',color=yel, $
      transpose([[replicate(xra(i mod 2,j),2)],$
                 [yra(*,j)],$
                 [replicate(zra(i/2,j),2)]]))
    oboundlines(i+8)=obj_new('IDLgrPolyline',color=yel, $
      transpose([[replicate(xra(i mod 2,j),2)],$
                 [replicate(yra(i/2,j),2)],$
                 [zra(*,j)]]))
  endfor

  ;  scale boundary field lines to normalized units
  for i=0,11 do $
    oboundlines[i]->setproperty,xcoord_conv=xs,ycoord_conv=ys,zcoord_conv=zs

  ;  add to model
  omodelin(j)->add,oboundlines

  ;  add extra objects
  if nextra gt 0 then begin
    case 1 of
      keyword_set(add_extra_objects_to_all): begin
        for k=0,nextra-1 do begin
          extra_objects(k)->setproperty,xcoord_conv=xs,ycoord_conv=ys,$
            zcoord_conv=zs
          omodelin(j)->add,extra_objects(k),/alias
        endfor
        end
      (nextra lt nview): begin
        nobj=j mod nextra
        extra_objects[nobj]->setproperty,xcoord_conv=xs,ycoord_conv=ys,$
        zcoord_conv=zs
        omodelin(j)->add,extra_objects(nobj),/alias
        end
      else: begin
        nobj=where((indgen(nextra) mod nview) eq j,numobj)
        for k=0,numobj-1 do begin
          extra_objects[nobj(k)]->setproperty,xcoord_conv=xs,ycoord_conv=ys,$
          zcoord_conv=zs
          omodelin(j)->add,extra_objects(nobj(k)),/alias
        endfor
        end
    endcase
  endif

endfor

;  create holder containing trackball and view (for easy destruction)
otrack=obj_new('Trackball',0.5*[xwid,ywid],0.5*max([xwid,ywid]))
ovgr=obj_new('IDLgrViewgroup')
ovgr->add,otrack
ovgr->add,oview

;  realize widget
widget_control,master,/realize

;  get id of window object
widget_control,wdraww,get_value=owindow

;  add graphics tree
owindow->setproperty,graphics_tree=ovgr

;  get size of top level base
widget_control,master,tlb_get_size=twid

;  save state of widget (o=object,w=widget,b=mousebutton,v=view,t=TLB)
state={owindow:owindow,oview:oview,omodelin:omodelin,otrack:otrack,ovgr:ovgr,$
       omodeltop:omodeltop,wdraw:wdraww,wrotl:wrotl,$
       bleft:0b,bright:0b,bdowny:0l,bdownzoom:1.0,bdownview:viewrect,$
       bmode:bmode,$
       vwid:[xwid,ywid],vwidv:[xwidv,ywidv],vr:viewrect,vzoom:1.0,$
       vmulti:multiview,$
       twid:twid,nview:nview}
widget_control,master,set_uval=state,/no_copy

;  relinquish control to manager
xmanager,'cart_trackball',master,/no_block

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_field_start_coord - This routine chooses gridpoints from which to
;                           trace the magnetic field we will later visualize
;
;  usage:  cart_field_start_coord,data,fieldtype,spacing,bbox=box,
;            radstart=radstart
;          where data = structure named cart_data_block, defined in
;                       cart_data_block__define.pro, that contains the
;                       cartesian field data
;                fieldtype = 1 = points are randomly distributed in x and y
;                            2 = uniform grid (default)
;                            3 = radial flux weighted at the start height
;                spacing = parameter controlling density of points, does
;                          different things depending on fieldtype
;                bbox = [x1,y1,x2,y2] defining bounding box in degrees
;                       outside of which no field line starting point can lie
;                radstart = a scalar equal to the vertical height at which all
;                           fieldlines should start (default=minimum z
;                           value in the domain)
;                add = set this flag if the new starting points are to be
;                      added to the existing set already defined in data
;
;          and inside the data structure:
;                (stx,sty,stz) = on output, coordinates of each starting point
;
;  M.DeRosa -  6 Dec 2004 - created, based on pfss_start_coord.pro
;             19 Feb 2005 - replaced data common block with data structure
;             15 Aug 2005 - fixed bug in fieldtype 2 when spacing is not an
;                           integer
;              1 Feb 2006 - added radstart keyword
;             17 Oct 2006 - added add keyword
;             10 Jun 2009 - added fieldtype 3
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_field_start_coord,data,fieldtype,spacing,bbox=box,$
  radstart=radstart,add=add

;  print usage message
if n_params() eq 0 then begin
  print,'  cart_field_start_coord,data,fieldtype,spacing,bbox=box,'+$
    'radstart=radstart,/add'
  return
endif

;  preliminaries
if n_elements(fieldtype) eq 0 then fieldtype=2 $
  else fieldtype=round(fieldtype(0))
if (fieldtype lt 1) or (fieldtype gt 3) then fieldtype=2

;  get mins,maxes
xmin=min(*data.xix,max=xmax)
ymin=min(*data.yix,max=ymax)
zmin=min(*data.zix,max=zmax)

;  set radstart if not set
if n_elements(radstart) eq 0 then radstart=zmin else radstart=radstart(0)

case fieldtype of

  1:  begin  ;  randomly distributed points

    npt=round(spacing(0))
    stx=randomu(seed,npt)*(xmax-xmin)+xmin
    sty=randomu(seed,npt)*(ymax-ymin)+ymin
    stz=replicate(radstart,npt)
    end

  2:  begin  ;  regular distribution of points

    nptx=round(data.nx/spacing)
    npty=round(data.ny/spacing)
    npt=nptx*npty
    xoffset=((xmax-xmin)/nptx)/2
    yoffset=((ymax-ymin)/npty)/2
    stx=reform( $
      linrange(nptx,xmin+xoffset,xmax-xoffset)#replicate(1,npty),npt,/over)
    sty=reform( $
      replicate(1,nptx)#linrange(npty,ymin+yoffset,ymax-yoffset),npt,/over)
    stz=replicate(radstart,npt)
    end

  3:  begin  ;  flux weighted

    ;  get random starting points, oversample by a factor of 10
    npt=round(spacing(0))
    oversampling=10
    stx=randomu(seed,npt*oversampling)*(xmax-xmin)+xmin
    sty=randomu(seed,npt*oversampling)*(ymax-ymin)+ymin
    stz=replicate(radstart,npt*oversampling)

    ;  determine Bz at each of the starting points
    ix=get_interpolation_index(*data.xix,stx)
    iy=get_interpolation_index(*data.yix,sty)
    iz=get_interpolation_index(*data.zix,stz)
    bz0=interpolate(*data.bz,ix,iy,iz)

    ;  reverse sort |Bz| values and choose points
    bzmagix=(reverse(sort(abs(bz0))))(0:npt-1)
    stx=stx(bzmagix)
    sty=sty(bzmagix)
    stz=stz(bzmagix)
    
    end

  else: print,'  cart_field_start_coord:  invalid fieldtype'

endcase

;  nullify pointers and save old values
if keyword_set(add) then begin
  addvalid=1
  if ptr_valid(data.stx) then oldstx=*data.stx else addvalid=0
  if ptr_valid(data.sty) then oldsty=*data.sty else addvalid=0
  if ptr_valid(data.stz) then oldstz=*data.stz else addvalid=0
endif else addvalid=0
if ptr_valid(data.stx) then ptr_free,data.stx
if ptr_valid(data.sty) then ptr_free,data.sty
if ptr_valid(data.stz) then ptr_free,data.stz

;  assign pointers
if (keyword_set(add) and addvalid) then begin
  data.stx=ptr_new([oldstx,stx])
  data.sty=ptr_new([oldsty,sty])
  data.stz=ptr_new([oldstz,stz])
endif else begin
  data.stx=ptr_new(stx)
  data.sty=ptr_new(sty)
  data.stz=ptr_new(stz)
endelse

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  trackball_wrapper.pro - Creates a widget application that allows the user
;                          to manipulate a user-supplied view(s) via IDL's
;                          trackball widget combined with object graphics.
;                          Multiple views (each of which individually respond
;                          to the trackball inputs) can be accommodated by
;                          providing arrays of each object type.
;
;  usage:  trackball_wrapper,object_list
;          where object_list = a single structure containing the following
;                              three tags (listed in hierarchical order):
;                  oview = the view(s)
;                  omodeltop = container(s) for the inner models
;                  omodelin = the model(s) containing all of the graphical
;                             objects to be rendered
;                multiview = [ncol,nrow] array describing placement of the
;                            multiple datasets in the widget, defaults to 2
;                            rows if not provided, unless there is only one
;                            dataset in which case it occupies the whole
;                            widget
;
;  notes: -cart_view_create.pro will create a structure that works here
;
;  M.DeRosa - 11 Jun 2008 - created, much cannibalized from my cartesian and
;                           spherical trackball routines
;             16 Jun 2008 - added capability to pan as well as rotate
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_trackball_event,event

;  get event ID and widget state
widget_control,event.id,get_uval=uval

if event.id eq event.top then begin  ;  TLB resize event

  ;  redefine state
  state=uval
  
  ;  get current TLB size
  widget_control,event.top,tlb_get_size=newwid

  ;  compute difference between current and old TLB size
  dwid=newwid-state.twid

  ;  set new draw window size
  state.vwid=state.vwid+dwid

  ;  resize the draw widget
  widget_control,state.wdraw,draw_x=state.vwid(0),draw_y=state.vwid(1)

  ;  compute new subwindow sizes
  state.vwidv=state.vwid/state.vmulti  ;  integer division

  ;  compute viewplane rectangle based on aspect ratio.
  aspect=float(state.vwidv(0))/float(state.vwidv(1))
  viewrect=[-1,-1,2,2]/sqrt(3.0)  ;  at first view spans -1/2 to +1/2 in x,y
  if aspect gt 1 then begin
    viewrect(0)=viewrect(0)-((aspect-1.0)*viewrect(2))/2.0
    viewrect(2)=viewrect(2)*aspect
  endif else begin
    viewrect(1)=viewrect(1)-(((1.0/aspect)-1.0)*viewrect(3))/2.0
    viewrect(3)=viewrect(3)/aspect
  endelse
  for i=0,state.nview-1 do state.oview(i)->setproperty,viewplane=viewrect, $
    dim=state.vwidv,loc=[(i mod state.vmulti(0))*state.vwidv(0),$
      (state.vmulti(1)-i/state.vmulti(0)-1)*state.vwidv(1)]

  ;  redraw view    
  state.owindow->draw

  ;  reset trackball
  state.otrack->reset,0.5*state.vwid,0.5*max(state.vwid)

endif else begin

  ;  get state
  widget_control,event.top,get_uval=state,/no_copy

  case (strsplit(uval,':',/extract))(0) of

    'EXIT': begin  ;  exit button pressed
      state.ovgr->remove,state.oview  ;  dissociate views from container
      for j=0,state.nview-1 do begin  ;  reset to standard view
        state.omodelin(j)->reset
        state.omodelin(j)->rotate,[1,0,0],-90
        state.omodelin(j)->rotate,[0,1,0],30
        state.omodelin(j)->rotate,[1,0,0],30
        state.omodeltop(j)->reset
      endfor
      obj_destroy,state.ovgr
      widget_control,event.top,/destroy
      return
      end

    'DRAW': begin  ;  (re)draw view

      ;  deal with buttons
      if state.bmode ne 'zoom' then begin

        ;  determine whether we are panning or rotating
        if state.bmode eq 'pan' then translate=1b else translate=0b

        ;  handle trackball updates
        newview=state.otrack->update(event,transform=tmatrix,transl=translate)
        if newview gt 0 then begin
          if translate then begin  ;  apply translation to top model
            state.omodeltop->getproperty,transform=oldt
            state.omodeltop->setproperty,transform=oldt#tmatrix
          endif else begin  ;  apply rotation to inner models
            for i=0,state.nview-1 do begin
              state.omodelin(i)->getproperty,transform=oldt
              state.omodelin(i)->setproperty,transform=oldt#tmatrix
            endfor

            ;  compute rotation angles
            state.omodelin(0)->getproperty,transform=tmatrix
            yang=asin(tmatrix(2))
            if abs(cos(yang)) gt 0.005 then begin
              xang=atan(-tmatrix(6),tmatrix(10))
              zang=atan(-tmatrix(1),tmatrix(0))
            endif else begin
              xang=0.0
              zang=atan(tmatrix(4),tmatrix(5))
            endelse
            outstr='('+string(xang*!radeg,f='(i4)')+','+ $
              string(yang*!radeg,f='(i4)')+','+string(zang*!radeg,f='(i4)')+')'
            widget_control,state.wrotl,set_val='rot.ang.(x,y,z)='+outstr

          endelse

        endif

      endif

      ;  handle mouse button events within draw widget
      case event.type of  ;  button down
        0: begin
          widget_control,state.wdraw,/draw_motion_events
          state.bdowny=event.y
          state.oview(0)->getproperty,viewplane=bdv  &  state.bdownview=bdv
          case event.press of
            1b: state.bleft=1b
            4b: state.bright=1b
            else:
          endcase
          end
        1: begin   ;  button up
          widget_control,state.wdraw,draw_motion_events=0
          state.bleft=0b  &  state.bright=0b
          end
        2: begin  ;  adjust zoom
          if (state.bright) or ((state.bleft) and (state.bmode eq 'zoom')) $
            then begin
            for i=0,state.nview-1 do state.oview(i)->setproperty,viewpl= $
              state.bdownview*exp((event.y-state.bdowny)/float(state.vwid(1)))
          endif
          end
        else:  ;  pass through
      endcase

      ;  (re)draw view    
      state.owindow->draw

      end

    'SAVE': begin  ;  save to disk

      ;  (re)draw view    
      state.owindow->draw

      ;  get snapshot of draw window
      state.owindow->getproperty,image_data=outim

      ;  determine filename
      ftype=(strsplit(uval,':',/extract))(1)
      fname=dialog_pickfile(file='cart_trackball.'+strlowcase(ftype),/write)

      ;  save to disk
      if fname ne '' then begin
        case ftype of
          'GIF': write_gif,fname,color_quan(outim,1,r,g,b),r,g,b
          'JPG': write_jpeg,fname,outim,/true,q=90
          'PNG': write_png,fname,outim
          'TIF': write_tiff,fname,reverse(outim,3)
          'EPS': begin  ;  clipboard idea from D.Fanning  (thanks David!)
            state.owindow->getproperty,dim=dim,units=units
            res300=replicate(2.54/300,2)  ;  300 DPI
            clip=obj_new('IDLgrClipboard',dim=dim,res=res,units=units,q=2)
            clip->draw,state.ovgr,file=fname,/postscript,/vector
            obj_destroy,clip
            end
          else:  ;  shouldn't be able to get here
        endcase
      endif

      end

    'MOUSE': begin  ;  mouse behavior events

      ;  determine what happened
      case (strsplit(uval,':',/extract))(1) of
        'PAN': state.bmode='pan'
        'ZOOM': state.bmode='zoom'
        else: state.bmode='rot'
      endcase
      
      ;  (re)draw view    
      state.owindow->draw

      end

    else: stop  ;  unrecognized event...

  endcase

  ;  set state
  widget_control,event.top,set_uval=state,/no_copy      

endelse

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro trackball_wrapper,object_list,multiview

;  usage message
if n_elements(object_list) eq 0 then begin
  print,'  trackball_wrapper,object_list[,multiview]'
  return
endif

;  get number of views
nview=n_elements(object_list.oview)
if nview eq 1 then multiview=[1,1] else $
  if n_elements(multiview) ne 2 then multiview=[nview/2,2]  ;  [col,row]
nview=nview<round(product(multiview))

;  set initial window size
xwid=round(400*multiview(0)) < 1000
ywid=round(400*multiview(1)) <  600

;  set subwindow sizes
xwidv=xwid/multiview(0)  ;  integer division
ywidv=ywid/multiview(1)  ;  integer division

;  compute viewplane rectangle based on aspect ratio
aspect=float(xwidv)/float(ywidv)  ;  aspect ratio of subwindow
viewrect=[-1,-1,2,2]/sqrt(3.0)
if aspect gt 1 then begin
  viewrect(0)=viewrect(0)-((aspect-1.0)*viewrect(2))/2.0
  viewrect(2)=viewrect(2)*aspect
endif else begin
  viewrect(1)=viewrect(1)-(((1.0/aspect)-1.0)*viewrect(3))/2.0
  viewrect(3)=viewrect(3)/aspect
endelse

;  loop through each input model and set the dimensions, location, viewplane
for i=0,nview-1 do begin

  ;  set viewplane
  object_list.oview(i)->setproperty,viewplane=viewrect

  ;  set location
  object_list.oview(i)->setproperty,location= $
    [(i mod multiview(0))*xwidv,(multiview(1)-i/multiview(0)-1)*ywidv]

  ;  set location
  object_list.oview(i)->setproperty,dimensions=[xwidv,ywidv]

endfor

;  create the widget
master=widget_base(/col,/base_align_left,/tlb_size_events,mbar=wmbar, $
  title='Field Line Renderer')

;  file menu
version=float(!version.release)  ;  for GIF availability test (below)
wmenuf=widget_button(wmbar,val='File ',/menu)
wsave=widget_button(wmenuf,val='Save as ...',/menu)
weps=widget_button(wsave,val='EPS',uval='SAVE:EPS')
if (version le 5.3) or (version ge 6.1) then $
  wgif=widget_button(wsave,val='GIF',uval='SAVE:GIF')
wjpeg=widget_button(wsave,val='JPEG',uval='SAVE:JPG')
wpng=widget_button(wsave,val='PNG',uval='SAVE:PNG')
wtiff=widget_button(wsave,val='TIFF',uval='SAVE:TIF')
wexitb=widget_button(wmenuf,val='Exit',uval='EXIT')

;  options under menu
woptions=widget_base(master,/row,/base_align_center)

;  rotate/pan button select
wpanb=widget_base(woptions,/row,/exclusive,frame=2)
wclickrot=widget_button(wpanb,val='rotate',uval='MOUSE:ROTATE')
wclickpan=widget_button(wpanb,val='pan',uval='MOUSE:PAN')
wclickpan=widget_button(wpanb,val='zoom',uval='MOUSE:ZOOM')
widget_control,wclickrot,/set_button  &  bmode='rot'

;  rotation angle info
wrotb=widget_base(woptions,/row)
wrotl=widget_label(wrotb,val='                                ')

;  draw window
wdrawb=widget_base(master)
wdraww=widget_draw(wdrawb,xsize=xwid,ysize=ywid,uvalue='DRAW',retain=0, $
  /expose_events,/button_events,graphics_level=2);,/color_model)

;  create holder containing trackball and view (for easy destruction)
otrack=obj_new('Trackball',0.5*[xwid,ywid],0.5*max([xwid,ywid]))
ovgr=obj_new('IDLgrViewgroup')
ovgr->add,otrack
ovgr->add,object_list.oview

;  realize widget
widget_control,master,/realize

;  get id of window object
widget_control,wdraww,get_value=owindow

;  add graphics tree
owindow->setproperty,graphics_tree=ovgr

;  get size of top level base
widget_control,master,tlb_get_size=twid

;  save state of widget (o=object,w=widget,b=mousebutton,v=view,t=TLB)
state={owindow:owindow,oview:object_list.oview,omodelin:object_list.omodelin, $
       otrack:otrack,ovgr:ovgr,omodeltop:object_list.omodeltop,wdraw:wdraww, $
       wrotl:wrotl,bleft:0b,bright:0b,bdowny:0l,bdownview:viewrect,$
       bmode:bmode,$
       vwid:[xwid,ywid],vwidv:[xwidv,ywidv],vr:viewrect,vmulti:multiview, $
       twid:twid,nview:nview}
widget_control,master,set_uval=state,/no_copy

;  relinquish control to manager
xmanager,'cart_trackball',master,/no_block

end

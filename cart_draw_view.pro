;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_draw_view - This routine draws an image of a view, defined using IDL's
;                   object graphics capabilities, for example, using
;                   cart_view_create.pro.  
;
;  usage:  cart_draw_view,obj_data,multi,xsize=xsize,ysize=ysize,
;            ax=ax,ay=ay,az=az,panx=panx,pany=pany,mag=mag,file=file,
;            outim=outim,/onscreen,/for_ps,/quiet,
;          where obj_list =  a single structure containing the following
;                              three tags (listed in hierarchical order):
;                  oview = the view(s)
;                  omodeltop = container(s) for the inner models
;                  omodelin = the model(s) containing all of the graphical
;                             objects to be rendered
;                multi = [ncol,nrow] array describing placement of multiple
;                        datasets, defaults to 2 rows if not provided, unless
;                        there is only one dataset in which case it occupies
;                        the whole view
;                xsize = number of pixels in the width of the space allocated 
;                        to one box (default=400, max=1000)
;                ysize = number of pixels in the height of the space allocated
;                        to one box (default=400, max=800)
;                [ax,ay,az] = angles of rotation about the x,y,and z axes in
;                             degrees, with rotation about the z axis
;                             occurring first, followed by rotation about the
;                             x axis and then rotation about the y axis
;                             (default=0 degrees for both, i.e. top-down view)
;                [panx,pany] = distance to pan view in the [x,y]-directions
;                mag = magnification factor
;                file = if set, FITS files of image(s) are created and no
;                       screen output is displayed, routine automatically
;                       adds .fits extension to filename and sets onscreen to
;                       false
;                outim = on output, image of z-buffer is read into this
;                        variable
;                onscreen = if set, then display image onscreen
;                for_ps = if set, then interchange white and black colors
;                quiet = set to inhibit screen output
;                help = set to display calling syntax
;
;  M.DeRosa - 22 Jun 2008 - created, patterened after trackball_wrapper.pro 
;                           and cart_draw_field2.pdf
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro cart_draw_view,obj_list,multiview,xsize=xsize,ysize=ysize, $
  ax=ax,ay=ay,az=az,panx=panx,pany=pany,mag=mag,file=file,outim=outim, $
  onscreen=onscreen,for_ps=for_ps,quiet=quiet,help=help

;  usage message
if keyword_set(help) or n_params() eq 0 then begin
  print,'  cart_draw_view,obj_list,multiview,xsize=xsize,ysize=ysize,'+$
    'ax=ax,ay=ay,az=az,panx=panx,pany=pany,mag=mag,file=file,outim=outim,'+$
    '/for_ps,/quiet'
  return
endif

;  some error checking
if n_elements(xsize) ne 1 then xsize=512 else xsize=xsize(0)
if n_elements(ysize) ne 1 then ysize=512 else ysize=ysize(0)
if n_elements(ax) eq 0 then ax=0.
if n_elements(ay) eq 0 then ay=0.
if n_elements(az) eq 0 then az=0.
if n_elements(panx) eq 0 then panx=0.0 else panx=panx(0)
if n_elements(pany) eq 0 then pany=0.0 else pany=pany(0)
if n_elements(mag) eq 0 then mag=1 else mag=mag(0)
if n_elements(file) ne 0 then begin
  onscreen=0
  if size(file,/type) ne 7 then file='test'
endif

;  get number of views
nview=n_elements(obj_list.oview)
if nview eq 1 then multiview=[1,1] else $
  if n_elements(multiview) ne 2 then multiview=[2,nview/2]  ;  [col,row]
nview=nview<round(product(multiview))

;  set initial window size
if n_elements(xsize) ne 1 then xwid=round(400*multiview(0)) < 1000 $
   else xwid=xsize(0)
if n_elements(ysize) ne 1 then ywid=round(400*multiview(1)) <  800 $
   else ywid=ysize(0)

;  set subwindow sizes
xwidv=xwid/multiview(0)  ;  integer division
ywidv=ywid/multiview(1)  ;  integer division

;  compute viewplane rectangle based on aspect ratio
aspect=float(xwidv)/float(ywidv)  ;  aspect ratio of subwindow
viewrect=[-1,-1,2,2]/sqrt(3.0)
if aspect gt 1 then begin
  viewrect(0)=viewrect(0)-((aspect-1.0)*viewrect(2))/2.0
  viewrect(2)=viewrect(2)*aspect
endif else begin
  viewrect(1)=viewrect(1)-(((1.0/aspect)-1.0)*viewrect(3))/2.0
  viewrect(3)=viewrect(3)/aspect
endelse

;  pan models
viewrect(0:1)=viewrect(0:1)+[panx,pany]
viewrect=viewrect/mag

;  loop through each input model and set the dimensions, location, viewplane
for i=0,nview-1 do begin

  ;  set viewplane
  obj_list.oview(i)->setproperty,viewplane=viewrect

  ;  set location
  obj_list.oview(i)->setproperty,location= $
    [(i mod multiview(0))*xwidv,(multiview(1)-i/multiview(0)-1)*ywidv]

  ;  set location
  obj_list.oview(i)->setproperty,dimensions=[xwidv,ywidv]

  ;  rotate models to desired views
  obj_list.omodelin(i)->reset
  obj_list.omodelin(i)->rotate,[0,0,1],az
  obj_list.omodelin(i)->rotate,[1,0,0],ax
  obj_list.omodelin(i)->rotate,[0,1,0],ay
  obj_list.omodeltop(i)->reset

endfor

;  render the image and read it out to an image array
obuffer=obj_new('IDLgrBuffer',dim=[xsize,ysize])
for i=0,nview-1 do obuffer->draw,obj_list.oview(i)
oimage=obuffer->read()
oimage->getproperty,data=outim

;  reset to standard view
for j=0,nview-1 do begin  ;  reset to standard view
  obj_list.omodelin(j)->reset
  obj_list.omodelin(j)->rotate,[1,0,0],-90
  obj_list.omodelin(j)->rotate,[0,1,0],30
  obj_list.omodelin(j)->rotate,[1,0,0],30
  obj_list.omodeltop(j)->reset
endfor

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  cart_get_curl.pro - This function returns the curl of a vector
;
;  usage:  result = cart_get_curl(xix,yix,zix,vecx,vecy,vecz,scheme=scheme)
;          where result = curl of input data
;                xix = x index array
;                yix = y index array
;                zix = z index array
;                vecx = input data, either a single array dimensioned
;                       (nx,ny,nz,3,nit) with the three planes are arranged
;                       (x,y,z), or the x-component of the vector field for
;                       which the curl is desired
;                vecy = the y-component of the vector field for which the
;                       curl is desired, required if only the
;                       x-component is in vecx
;                vecz = the z-component of the vector field for which the
;                       curl is desired, required if only the
;                       x-component is in vecx
;                scheme = derivative scheme, passed through to dbyd1
;
;  M.DeRosa - 9 Dec 2004 - created
;             1 Mar 2005 - now accepts input vector in component form
;             9 May 2006 - added scheme keyword
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
function cart_get_curl,xix,yix,zix,vecx,vecy,vecz,scheme=scheme

;  usage_message, parameter check
case n_params() of
  4:  comp=0  ;  input not in component form
  6:  comp=1  ;  input is in component form
  else: begin
    print,'  result=cart_get_curl(xix,yix,zix,vecx,vecy,vecz,scheme=scheme)'
    return,-1
    end
endcase

;  check to see if dimensioned properly
nax=size(vecx,/dim)
nx=n_elements(xix)
ny=n_elements(yix)
nz=n_elements(zix)
if nx ne nax(0) then begin
  print,'  cart_get_curl:  inputs xix and vector not compatible'
  return,-1
endif
if ny ne nax(1) then begin
  print,'  cart_get_curl:  inputs yix and vector not compatible'
  return,-1
endif
if nz ne nax(2) then begin
  print,'  cart_get_curl:  inputs zix and vector not compatible'
  return,-1
endif
if comp then begin
  if total(size(vecx,/dim) ne size(vecy,/dim)) gt 0 then begin
    print,'  cart_get_curl:  vecx and vecy inputs are not dimensioned the same'
    return,-1
  endif
  if total(size(vecx,/dim) ne size(vecz,/dim)) gt 0 then begin
    print,'  cart_get_curl:  vecx and vecz inputs are not dimensioned the same'
    return,-1
  endif
endif

;  determine number of fields, and dimension result
if comp then begin
  if n_elements(nax) eq 4 then nit=nax(3) else nit=1
endif else begin
  if n_elements(nax) eq 5 then nit=nax(4) else nit=1
endelse
res=fltarr(nx,ny,nz,3,nit,/noz)

;  compute curl
if comp then begin

  for i=0,nit-1 do begin

    ;  x-derivatives
    for iz=0,nz-1 do for iy=0,ny-1 do begin
      res(*,iy,iz,2,i)=dbyd1(scheme=scheme,reform(vecy(*,iy,iz,i)),xix)
      res(*,iy,iz,1,i)=-dbyd1(scheme=scheme,reform(vecz(*,iy,iz,i)),xix)
    endfor

    ;  y-derivatives
    for iz=0,nz-1 do for ix=0,nx-1 do begin
      res(ix,*,iz,0,i)=dbyd1(scheme=scheme,reform(vecz(ix,*,iz,i)),yix)
      res(ix,*,iz,2,i)=res(ix,*,iz,2,i)-$
                       dbyd1(scheme=scheme,reform(vecx(ix,*,iz,i)),yix)
    endfor

    ;  z-deriv
    for iy=0,ny-1 do for ix=0,nx-1 do begin
      res(ix,iy,*,1,i)=res(ix,iy,*,1,i)+$
                       dbyd1(scheme=scheme,reform(vecx(ix,iy,*,i)),zix)
      res(ix,iy,*,0,i)=res(ix,iy,*,0,i)-$
                       dbyd1(scheme=scheme,reform(vecy(ix,iy,*,i)),zix)
    endfor

  endfor

endif else begin

  for i=0,nit-1 do begin

    ;  x-derivatives
    for iz=0,nz-1 do for iy=0,ny-1 do begin
      res(*,iy,iz,2,i)=dbyd1(scheme=scheme,reform(vecy(*,iy,iz,1,i)),xix)
      res(*,iy,iz,1,i)=-dbyd1(scheme=scheme,reform(vecz(*,iy,iz,2,i)),xix)
    endfor

    ;  y-derivatives
    for iz=0,nz-1 do for ix=0,nx-1 do begin
      res(ix,*,iz,0,i)=dbyd1(scheme=scheme,reform(vecz(ix,*,iz,2,i)),yix)
      res(ix,*,iz,2,i)=res(ix,*,iz,2,i)-$
                       dbyd1(scheme=scheme,reform(vecx(ix,*,iz,0,i)),yix)
    endfor

    ;  z-deriv
    for iy=0,ny-1 do for ix=0,nx-1 do begin
      res(ix,iy,*,1,i)=res(ix,iy,*,1,i)+$
                       dbyd1(scheme=scheme,reform(vecx(ix,iy,*,0,i)),zix)
      res(ix,iy,*,0,i)=res(ix,iy,*,0,i)-$
                       dbyd1(scheme=scheme,reform(vecy(ix,iy,*,1,i)),zix)
    endfor

  endfor

endelse

return,reform(res,/over)

end
